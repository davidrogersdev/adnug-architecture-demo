﻿using AutoMapper;

namespace Adnug.Arch.Core.Mapping
{
    public interface IConfigureThisMapping
    {
        void CreateMappings(Profile configuration);
    }
}
