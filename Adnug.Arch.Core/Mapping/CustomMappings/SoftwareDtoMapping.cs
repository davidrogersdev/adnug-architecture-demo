﻿using Adnug.Arch.Core.DataTransferObjects;
using Adnug.Arch.DataAccess.DomainModels;
using AutoMapper;

namespace Adnug.Arch.Core.Mapping.CustomMappings
{
    public class SoftwareDtoMapping : IConfigureThisMapping
    {
        public void CreateMappings(Profile configuration)
        {
            configuration.CreateMap<Software, SoftwareDto>()
                .ForMember(dest => dest.SoftwareType, src => src.Ignore())
                .ForMember(dest => dest.NameAndDescription, src => src.MapFrom(s => s.Name + ": " + s.Description));
        }
    }
}
