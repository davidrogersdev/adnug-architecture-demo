﻿using Adnug.Arch.Core.DataTransferObjects;
using Adnug.Arch.DataAccess.DomainModels;
using AutoMapper;

namespace Adnug.Arch.Core.Mapping.CustomMappings
{
    public class LicenceAllocationDtoMapping : IConfigureThisMapping
    {
        public void CreateMappings(Profile configuration)
        {
            configuration.CreateMap<LicenceAllocation, LicenceAllocationDto>()
                .PreserveReferences();
        }
    }
}
