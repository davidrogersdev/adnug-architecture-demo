﻿namespace Adnug.Arch.Core.Mapping
{
    public interface IHaveCustomMappings<T>
		where T : IConfigureThisMapping
    {
        // Marker interface. Deliberately empty. https://en.wikipedia.org/wiki/Marker_interface_pattern
    }
}
