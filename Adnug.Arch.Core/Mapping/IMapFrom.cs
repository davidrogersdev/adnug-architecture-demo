﻿namespace Adnug.Arch.Core.Mapping
{
    public interface IMapFrom<T>
    {
        // Marker interface. Deliberately empty. https://en.wikipedia.org/wiki/Marker_interface_pattern
    }
}