﻿using Adnug.Arch.Core.DataTransferObjects;

namespace Adnug.Arch.Core.ApplicationServices
{
    public interface ILicenceDataService
    {
        LicenceDto GetLicence(int id);
        int UpdateLicence(LicenceDto licenceDto);
    }
}