﻿using System.Data.Entity;
using System.Linq;
using Adnug.Arch.Core.DataTransferObjects;
using Adnug.Arch.Core.Infrastructure;
using Adnug.Arch.DataAccess.Db;
using Adnug.Arch.DataAccess.DomainModels;
using AutoMapper;
using FluentValidation;

namespace Adnug.Arch.Core.ApplicationServices
{
    public class LicenceDataService : ILicenceDataService
    {
        private readonly IDbContextFoundary _dbContextFoundary;
        private readonly IMapper _mapper;
        private readonly IValidator<LicenceDto> _validator;

        public LicenceDataService(IDbContextFoundary dbContextFoundary, IMapper mapper, IValidator<LicenceDto> validator)
        {
            _dbContextFoundary = dbContextFoundary;
            _mapper = mapper;
            _validator = validator;
        }


        public LicenceDto GetLicence(int id)
        {
            var context = _dbContextFoundary.GetCurrentContext();

            var licence = context.Licences
                .Include(l => l.Software)
                .Include(l => l.LicenceAllocations)
                .FirstOrDefault(l => l.Id == id);

            if (licence == null)
                return null;

            return _mapper.Map<Licence, LicenceDto>(licence);
        }

        public int UpdateLicence(LicenceDto licenceDto)
        {
            _validator.ValidateAndThrow(licenceDto); // 📌 frivolous example just to demonstrate usage 

            var context = _dbContextFoundary.GetCurrentContext();

            var licence = context.Licences.Include(l => l.Software).Single(l => l.Id == licenceDto.Id);
            licence.LicenceKey = licenceDto.LicenceKey.Trim();
            licence.Software.Name = licenceDto.Software.NameAndDescription.SubstringUpToFirst(":").Trim();
            licence.Software.Description = licenceDto.Software.NameAndDescription.SubstringFollowing(":").Trim();

            return context.SaveChanges() > 0 ? licence.Id : -1;
        }
    }
}
