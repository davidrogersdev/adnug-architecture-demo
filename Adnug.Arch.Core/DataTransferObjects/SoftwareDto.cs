﻿using Adnug.Arch.Core.Mapping;
using Adnug.Arch.Core.Mapping.CustomMappings;
using Adnug.Arch.DataAccess.DomainModels;

namespace Adnug.Arch.Core.DataTransferObjects
{
    public class SoftwareDto : IHaveCustomMappings<SoftwareDtoMapping>
    {
        public int Id { get; set; }
        public string NameAndDescription { get; set; }
        public int SoftwareTypeId { get; set; }
        public virtual SoftwareType SoftwareType { get; set; }
    }
}