﻿using System;
using Adnug.Arch.Core.Mapping;
using Adnug.Arch.Core.Mapping.CustomMappings;

namespace Adnug.Arch.Core.DataTransferObjects
{
    public class LicenceAllocationDto : IHaveCustomMappings<LicenceAllocationDtoMapping>
    {
        public int Id { get; set; }
        public int PersonId { get; set; }
        public int LicenceId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public virtual LicenceDto Licence { get; set; }
    }
}
