﻿using System.Collections.Generic;
using Adnug.Arch.Core.Mapping;
using Adnug.Arch.DataAccess.DomainModels;

namespace Adnug.Arch.Core.DataTransferObjects
{
    public class LicenceDto : IMapFrom<Licence>
    {
        public int Id { get; set; }
        public string LicenceKey { get; set; }
        public int SoftwareId { get; set; }
        public IEnumerable<LicenceAllocationDto> LicenceAllocations { get; set; }
        public SoftwareDto Software { get; set; }
    }
}
