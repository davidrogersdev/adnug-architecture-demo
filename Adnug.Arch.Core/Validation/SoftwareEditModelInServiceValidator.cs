using System;
using Adnug.Arch.Core.DataTransferObjects;
using FluentValidation;

namespace Adnug.Arch.Core.Validation
{
    public class SoftwareEditModelInServiceValidator : AbstractValidator<LicenceDto>
    {
        public SoftwareEditModelInServiceValidator()
        {
            RuleFor(m => m.LicenceKey)
                .Must(LicenceKeyIsGuid)
                .WithMessage("Licence Key: {0} must be a valid Guid", m => m.LicenceKey);
        }

        private bool LicenceKeyIsGuid(LicenceDto licence, string licenceKey)
        {
            Guid outGuid; 

            return Guid.TryParse(licenceKey, out outGuid);
        }
    }
}