using System;

namespace Adnug.Arch.Core.Infrastructure
{
    public static class StringExtensions
    {
        public const string NotFound = "_NOT_FOUND_";

        public static TEnum ParseEnum<TEnum>(this string value)
            where TEnum : struct
        {
            return (TEnum)Enum.Parse(typeof(TEnum), value);
        }

        public static string SubstringFrom(this string source, string start, int? count = null)
        {
            if (string.IsNullOrEmpty(source))
                return NotFound;

            if (string.IsNullOrEmpty(start))
                throw new ArgumentException($"The \"{"start"}\" parameter cannot be null or an empty string.", "start");

            int indexOfString = source.IndexOf(start, StringComparison.Ordinal);

            return SubstringHelper(source, count, indexOfString);
        }

        public static string SubstringFollowing(this string source, string start, int? count = null)
        {
            if (string.IsNullOrEmpty(source))
                return NotFound;

            if (string.IsNullOrEmpty(start))
                throw new ArgumentException($"The \"{"start"}\" parameter cannot be null or an empty string.", "start");

            int indexOfString = source.IndexOf(start, StringComparison.Ordinal) + 1;

            return SubstringHelper(source, count, indexOfString);
        }

        public static string SubstringUpToFirst(this string source, string stopper)
        {
            if (string.IsNullOrEmpty(source))
                return NotFound;
            
            int indexOfString = source.IndexOf(stopper, StringComparison.OrdinalIgnoreCase);

            return source.Substring(0, indexOfString);
        }

        private static string SubstringHelper(string source, int? count, int indexOfString)
        {
            //  If the string is not contained in the source string at all, return the NotFound constant. The caller can guard against it.
            if (indexOfString < 0)
                return NotFound;

            if (!count.HasValue)
            {
                return source.Substring(indexOfString);
            }

            if (count.Value > 0)
            {
                return source.Substring(indexOfString, count.Value);
            }

            throw new Exception($"If the \"{"count"}\" parameter is not null, it must be greated than 0.");
        }

    }
}
