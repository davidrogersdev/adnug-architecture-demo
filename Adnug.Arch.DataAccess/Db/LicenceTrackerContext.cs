﻿using System.Data.Entity;
using Adnug.Arch.DataAccess.DomainModels;

namespace Adnug.Arch.DataAccess.Db
{
    public class LicenceTrackerContext : DbContext
    {
        static LicenceTrackerContext()
        {

        }

        public LicenceTrackerContext()
                : base("LicenceTrackerContext")
        {
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }
        
        public DbSet<Person> People { get; set; }
        public DbSet<Software> SoftwareProducts { get; set; }
        public DbSet<SoftwareFile> SoftwareFiles { get; set; }
        public DbSet<LicenceAllocation> LicenceAllocations { get; set; }
        public DbSet<Licence> Licences { get; set; }
        public DbSet<SoftwareType> SoftwareTypes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.ConfigureDomainModels();

            base.OnModelCreating(modelBuilder);
        }
    }
}
