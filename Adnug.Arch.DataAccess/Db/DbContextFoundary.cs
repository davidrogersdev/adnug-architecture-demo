﻿using SimpleInjector;

namespace Adnug.Arch.DataAccess.Db
{
    public class DbContextFoundary : IDbContextFoundary
    {
        private readonly Container _container;

        public DbContextFoundary(Container container)
        {
            _container = container;
        }

        public LicenceTrackerContext GetCurrentContext()
        {
            return _container.GetInstance<LicenceTrackerContext>();
        }
    }
}
