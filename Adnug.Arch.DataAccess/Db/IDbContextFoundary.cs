﻿namespace Adnug.Arch.DataAccess.Db
{
    public interface IDbContextFoundary
    {
        LicenceTrackerContext GetCurrentContext();
    }
}
