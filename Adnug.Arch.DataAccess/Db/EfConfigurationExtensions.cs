﻿using System.Data.Entity;
using Adnug.Arch.DataAccess.DomainModels.Mapping;

namespace Adnug.Arch.DataAccess.Db
{
    public static class EfConfigurationExtensions
    {
        public static void ConfigureDomainModels(this DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new LicenceAllocationMap());
            modelBuilder.Configurations.Add(new LicenceMap());
            modelBuilder.Configurations.Add(new PersonMap());
            modelBuilder.Configurations.Add(new SoftwareFileMap());
            modelBuilder.Configurations.Add(new SoftwareMap());
            modelBuilder.Configurations.Add(new SoftwareTypeMap());
        }
    }
}
