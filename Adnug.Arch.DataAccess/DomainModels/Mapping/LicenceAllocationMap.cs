﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Adnug.Arch.DataAccess.DomainModels.Mapping
{
    public class LicenceAllocationMap : EntityTypeConfiguration<LicenceAllocation>
    {
        public LicenceAllocationMap()
        {
            // Primary Key
            HasKey(l => l.Id);

            // Properties
            Property(l => l.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(l => l.PersonId).IsRequired();
            Property(l => l.LicenceId).IsRequired();
            Property(l => l.StartDate).IsRequired();

            // Relationships
            HasRequired(u => u.Licence)
                 .WithMany(u => u.LicenceAllocations)
                 .HasForeignKey(u => u.LicenceId)
                 .WillCascadeOnDelete(true);

            HasRequired(u => u.Person)
                 .WithMany(u => u.LicenceAllocations)
                 .HasForeignKey(u => u.PersonId)
                 .WillCascadeOnDelete(true);

        }
    }
}
