﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Adnug.Arch.DataAccess.DomainModels.Mapping
{
    public class SoftwareTypeMap : EntityTypeConfiguration<SoftwareType>
    {
        public SoftwareTypeMap()
        {
            // Primary Key
            HasKey(st => st.Id);

            // Properties
            Property(st => st.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(sf => sf.Name).IsRequired().IsVariableLength().HasMaxLength(250);
            Property(sf => sf.Description).IsOptional().IsVariableLength().HasMaxLength(250);

            // Relationships
            /* Dealt with by entity on other side of relationship */
        }
    }
}
