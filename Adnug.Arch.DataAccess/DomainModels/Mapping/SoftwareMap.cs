﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Adnug.Arch.DataAccess.DomainModels.Mapping
{
    public class SoftwareMap : EntityTypeConfiguration<Software>
    {
        public SoftwareMap()
        {
            // Primary Key
            HasKey(s => s.Id);

            // Properties
            Property(s => s.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(s => s.Name).IsRequired().IsVariableLength().HasMaxLength(null);
            Property(s => s.Description).IsOptional().HasMaxLength(250).IsVariableLength();
            Property(s => s.SoftwareTypeId).IsRequired();

            // Relationships
            HasRequired(u => u.SoftwareType)
                 .WithMany(u => u.Softwares)
                 .HasForeignKey(u => u.SoftwareTypeId)
                 .WillCascadeOnDelete(true);

        }
    }
}
