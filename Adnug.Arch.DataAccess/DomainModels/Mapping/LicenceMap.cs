﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Adnug.Arch.DataAccess.DomainModels.Mapping
{
    public class LicenceMap : EntityTypeConfiguration<Licence>
    {
        public LicenceMap()
        {
            // Primary Key
            HasKey(l => l.Id);

            // Properties
            Property(t => t.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(l => l.LicenceKey).HasMaxLength(250).IsRequired().IsVariableLength();
            Property(l => l.SoftwareId).IsRequired();

            // Relationships
            HasRequired(u => u.Software)
                 .WithMany(u => u.Licences)
                 .HasForeignKey(u => u.SoftwareId)
                 .WillCascadeOnDelete(true);
        }
    }
}
