﻿using System.Collections.Generic;

namespace Adnug.Arch.DataAccess.DomainModels
{
   public class Licence
    {
        public Licence()
        {
            LicenceAllocations = new List<LicenceAllocation>();
        }

        public int Id { get; set; }
        public string LicenceKey { get; set; }
        public int SoftwareId { get; set; }
        public virtual ICollection<LicenceAllocation> LicenceAllocations { get; set; }
        public virtual Software Software { get; set; }
    }
}
