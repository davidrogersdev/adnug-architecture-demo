﻿using System.Collections.Generic;

namespace Adnug.Arch.DataAccess.DomainModels
{
    public class Person
    {
        public Person()
        {
            LicenceAllocations = new List<LicenceAllocation>();
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public virtual ICollection<LicenceAllocation> LicenceAllocations { get; set; }
    }
}
