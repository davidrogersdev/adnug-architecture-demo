﻿using System;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Newtonsoft.Json;
using Adnug.Arch.Mvc.Infrastructure;

namespace Adnug.Arch.Mvc.ModelBinders
{
    public sealed class DynamicModelBinder : IModelBinder
    {
        private const string ContentType = UiConstants.JsonMimeType;

        //public DynamicModelBinder(bool useModelName = false)
        //{
        //    this.UseModelName = useModelName;
        //}

        //public bool UseModelName { get; private set; }

        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            dynamic data = null;

            if ((controllerContext.HttpContext.Request.AcceptTypes.Any(x => x.StartsWith(ContentType, StringComparison.OrdinalIgnoreCase)) &&
                controllerContext.HttpContext.Request.ContentType.StartsWith(ContentType, StringComparison.OrdinalIgnoreCase)))
            {
                controllerContext.HttpContext.Request.InputStream.Position = 0;

                using (var reader = new StreamReader(controllerContext.HttpContext.Request.InputStream))
                {
                    var payload = reader.ReadToEnd();

                    if (string.IsNullOrWhiteSpace(payload) == false)
                    {
                        data = JsonConvert.DeserializeObject(payload);

                        //if (UseModelName)
                        //{
                        //    data = data[bindingContext.ModelName];
                        //}
                    }
                }
            }

            return data;
        }
    }
}
