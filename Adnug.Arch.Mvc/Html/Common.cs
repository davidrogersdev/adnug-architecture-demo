﻿using System.Web.Mvc;

namespace Adnug.Arch.Mvc.Html
{
    public static class HtmlExtensionsCommon
    {
        public enum HtmlButtonTypes
        {
            submit,
            button,
            reset
        }

        public enum Html5InputTypes
        {
            text,
            color,
            date,
            datetime,
            email,
            month,
            number,
            password,
            range,
            search,
            tel,
            time,
            url,
            week
        }

        public static void AddName(TagBuilder tb, string name, string id)
        {
            if (!string.IsNullOrWhiteSpace(name))
            {
                name = TagBuilder.CreateSanitizedId(name);

                if (string.IsNullOrWhiteSpace(id))
                {
                    tb.GenerateId(name);
                }
                else
                {
                    tb.MergeAttribute("id", TagBuilder.CreateSanitizedId(id));
                }
            }

            tb.MergeAttribute("name", name);
        }

        public static string ProcessBootstrapButtonType(string cssClass)
        {
            if (!string.IsNullOrWhiteSpace(cssClass))
            {
                if (!cssClass.Contains("btn-"))
                {
                    cssClass = "btn-primary " + cssClass;
                }
            }
            else
            {
                cssClass = "btn-primary";
            }

            return cssClass;
        }
    }
}
