﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace Adnug.Arch.Mvc.Html
{
    public static class DropDownListExtensions
    {
        public static IHtmlString BootstrapDropDownListFor<TModel, TProp>(
            this HtmlHelper<TModel> helper, 
            Expression<Func<TModel, TProp>> property,
            IEnumerable<SelectListItem> selectList, 
            string optionLabel = null)
        {
            if (string.IsNullOrWhiteSpace(optionLabel))
            {
                return helper.DropDownListFor(property, selectList, new
                {
                    @class = "form-control"
                });
            }

            return helper.DropDownListFor(property, selectList, optionLabel, new
            {
                @class = "form-control"
            });
        }

        public static IHtmlString BootstrapDropDownList(
            this HtmlHelper helper, 
            string propertyName, 
            IEnumerable<SelectListItem> selectList, 
            string optionLabel = null)
        {
            if (string.IsNullOrWhiteSpace(optionLabel))
            {
                return helper.DropDownList(propertyName, selectList, new
                {
                    @class = "control-label"
                });
            }

            return helper.DropDownList(propertyName, selectList, optionLabel, new
            {
                @class = "control-label"
            });
        }
    }
}
