﻿using System;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace Adnug.Arch.Mvc.Html
{
    public static class LabelExtensions
    {
        public static IHtmlString BootstrapLabelFor<TModel, TProp>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TProp>> property)
        {
            return helper.LabelFor(property, new
            {
                @class = "control-label"
            });
        }

        public static IHtmlString BootstrapLabel(this HtmlHelper helper, string propertyName)
        {
            return helper.Label(propertyName, new
            {
                @class = "control-label"
            });
        }

    }
}
