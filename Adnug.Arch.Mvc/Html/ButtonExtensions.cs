﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Adnug.Arch.Mvc.Infrastructure;

namespace Adnug.Arch.Mvc.Html
{
    public static class ButtonExtensions
    {
        public static IHtmlString BootstrapButtonFor<TModel, TResult>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TResult>> model,
            string innerHtml,
            string action,
            string controller,
            string cssClass,
            string name,
            string title,
            bool isFormNoValidate,
            bool isAutoFocus,
            object htmlAttributes = null)
        {
            var buttonTagBuilder = new TagBuilder(UiConstants.Button);

            HtmlExtensionsCommon.ProcessBootstrapButtonType(cssClass);

            buttonTagBuilder.AddCssClass(cssClass);

            buttonTagBuilder.AddCssClass("btn");


            if (!string.IsNullOrWhiteSpace(title))
            {
                buttonTagBuilder.MergeAttribute(UiConstants.TitleAttribute, title);
            }

            if (isFormNoValidate)
            {
                buttonTagBuilder.MergeAttribute(UiConstants.FormNoValidateAttribute, UiConstants.FormNoValidateAttribute);
            }

            if (isAutoFocus)
            {
                buttonTagBuilder.MergeAttribute(UiConstants.AutoFocusAttribute, UiConstants.AutoFocusAttribute);
            }

            buttonTagBuilder.MergeAttributes(HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));

            HtmlExtensionsCommon.AddName(buttonTagBuilder, name, string.Empty);

            buttonTagBuilder.InnerHtml = innerHtml;

            var httpContext = htmlHelper.ViewContext.HttpContext;
            var user = (ClaimsPrincipal)httpContext.User;

            var routeData = RouteTable.Routes.GetRouteData(httpContext);
            var urlHelper = new UrlHelper(new RequestContext(httpContext, routeData));
            var url = urlHelper.Action(action, controller);

            if (model != null)
            {
                var propertyName = ExpressionHelper.GetExpressionText(model);

                var viewModel = htmlHelper.ViewData.Model;

                if (viewModel != null)
                {
                    var modelType = typeof(TModel);
                    var propertyInfo = modelType.GetProperty(propertyName);
                    var propertyValue =
                        propertyInfo.GetValue(viewModel, null) as Tuple<IDictionary<string, bool>, ISet<string>>;
                    var linksRequiringAuthentication = propertyValue.Item1;

                    if (propertyValue != null)
                    {
                        var authenticationIsRequired = linksRequiringAuthentication[url];

                        if (!user.Identity.IsAuthenticated)
                        {
                            // if not authentication, the link does not get rendered.
                            if (authenticationIsRequired)
                                return MvcHtmlString.Empty;
                        }
                        else
                        {
                            // Some links will only be visible if the User has an Role claim of "admin".
                            var linksRequiringAdminRole = propertyValue.Item2;

                            if (linksRequiringAdminRole.Contains(url))
                            {

                                if (!user.HasClaim("http://schemas.surgeons.or/2016/06/identity/claims/bqausertype", "admin"))
                                {
                                    return MvcHtmlString.Empty;
                                }
                            }
                        }
                    }
                }
            }

            buttonTagBuilder.MergeAttribute(UiConstants.HrefAttribute, url);

            return MvcHtmlString.Create(buttonTagBuilder.ToString());
        }

        public static IHtmlString BootstrapIconButton(
            this HtmlHelper htmlHelper,
            string icon,
            string buttonText,
            string name,
            bool isFormNoValidate,
            bool isAutoFocus,
            object htmlAttributes = null)
        {
            return htmlHelper.BootstrapIconButton(icon, buttonText, null, null, null, name, isAutoFocus, isFormNoValidate, htmlAttributes);
        }

        public static IHtmlString BootstrapIconButton(
            this HtmlHelper htmlHelper,
            string icon,
            string buttonText,
            string action,
            string controller,
            string cssClass,
            string name,
            bool isFormNoValidate,
            bool isAutoFocus,
            object htmlAttributes = null)
        {
            var buttonTagBuilder = new TagBuilder(UiConstants.Button);
            var iconTagBuilder = new TagBuilder(UiConstants.I);

            iconTagBuilder.AddCssClass(icon);

            buttonTagBuilder.AddCssClass(HtmlExtensionsCommon.ProcessBootstrapButtonType(cssClass));

            buttonTagBuilder.AddCssClass("btn");

            if (isFormNoValidate)
            {
                buttonTagBuilder.MergeAttribute(UiConstants.FormNoValidateAttribute, UiConstants.FormNoValidateAttribute);
            }

            if (isAutoFocus)
            {
                buttonTagBuilder.MergeAttribute(UiConstants.AutoFocusAttribute, UiConstants.AutoFocusAttribute);
            }

            buttonTagBuilder.MergeAttributes(HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));

            HtmlExtensionsCommon.AddName(buttonTagBuilder, name, string.Empty);

            buttonTagBuilder.InnerHtml = string.Concat(iconTagBuilder.ToString(TagRenderMode.Normal), UiConstants.NonBreakingSpace, buttonText);

            return MvcHtmlString.Create(buttonTagBuilder.ToString());
        }
    }
}
    