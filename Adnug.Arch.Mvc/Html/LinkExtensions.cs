﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Adnug.Arch.Mvc.Infrastructure;

namespace Adnug.Arch.Mvc.Html
{
    public static class LinkExtensions
    {
        public static IHtmlString BootstrapMembershipIconLinkButtonFor<TModel, TResult>(
          this HtmlHelper<TModel> htmlHelper,
          Expression<Func<TModel, TResult>> model,
          string innerHtml,
          string action,
          string controller,
          object htmlAttributes = null)
        {
            return htmlHelper.BootstrapMembershipIconLinkButtonFor(
                model,
                innerHtml,
                action,
                controller,
                null, 
                null, 
                null, 
                null, 
                false, 
                false, 
                htmlAttributes
                );
        }

        public static IHtmlString BootstrapMembershipIconLinkButtonFor<TModel, TResult>(
          this HtmlHelper<TModel> htmlHelper,
          Expression<Func<TModel, TResult>> model,
          string innerHtml,
          string action,
          string controller,
          string cssClass,
          object htmlAttributes = null)
        {
            return htmlHelper.BootstrapMembershipIconLinkButtonFor(
                model,
                innerHtml,
                action,
                controller,
                cssClass, 
                null,
                null, 
                null, 
                false, 
                false, 
                htmlAttributes
                );
        }

        public static IHtmlString BootstrapMembershipIconLinkButtonFor<TModel, TResult>(
          this HtmlHelper<TModel> htmlHelper,
          Expression<Func<TModel, TResult>> model,
          string innerHtml,
          string action,
          string controller,
          string cssClass,
          string font,
          object htmlAttributes = null)
        {
            return htmlHelper.BootstrapMembershipIconLinkButtonFor(
                model,
                innerHtml,
                action,
                controller,
                cssClass, 
                font,
                null, 
                null, 
                false, 
                false, 
                htmlAttributes
                );
        }

        public static IHtmlString BootstrapMembershipIconLinkButtonFor<TModel, TResult>(
          this HtmlHelper<TModel> htmlHelper,
          Expression<Func<TModel, TResult>> model,
          string innerHtml,
          string action,
          string controller,
          string cssClass,
          string font,
          string name,
          string title,
          bool isFormNoValidate,
          bool isAutoFocus,
          object htmlAttributes = null) 
        {
            var anchorTagBuilder = new TagBuilder(UiConstants.Anchor);
            TagBuilder iconTagBuilder = null;

            if (!string.IsNullOrWhiteSpace(font))
            {
                iconTagBuilder = new TagBuilder(UiConstants.I);
                iconTagBuilder.AddCssClass(font);
            }

            HtmlExtensionsCommon.ProcessBootstrapButtonType(cssClass);

            anchorTagBuilder.AddCssClass(cssClass);

            anchorTagBuilder.AddCssClass("btn");

            if (!string.IsNullOrWhiteSpace(title))
            {
                anchorTagBuilder.MergeAttribute(UiConstants.TitleAttribute, title);
            }

            if (isFormNoValidate)
            {
                anchorTagBuilder.MergeAttribute(UiConstants.FormNoValidateAttribute, UiConstants.FormNoValidateAttribute);
            }

            if (isAutoFocus)
            {
                anchorTagBuilder.MergeAttribute(UiConstants.AutoFocusAttribute, UiConstants.AutoFocusAttribute);
            }

            anchorTagBuilder.MergeAttributes(HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));

            HtmlExtensionsCommon.AddName(anchorTagBuilder, name, string.Empty);

            anchorTagBuilder.InnerHtml = iconTagBuilder == null 
                ? innerHtml
                : string.Concat(iconTagBuilder.ToString(TagRenderMode.Normal), UiConstants.NonBreakingSpace, innerHtml);

            var httpContext = htmlHelper.ViewContext.HttpContext;
            var user = (ClaimsPrincipal)httpContext.User;

            var routeData = RouteTable.Routes.GetRouteData(httpContext);
            var urlHelper = new UrlHelper(new RequestContext(httpContext, routeData));
            var url = urlHelper.Action(action, controller);
            
            if (model != null)
            {
                var propertyName = ExpressionHelper.GetExpressionText(model);

                var viewModel = htmlHelper.ViewData.Model;

                if (viewModel != null)
                {
                    var modelType = typeof(TModel);
                    var propertyInfo = modelType.GetProperty(propertyName);
                    var propertyValue = propertyInfo.GetValue(viewModel, null) as Tuple<IDictionary<string, bool>, ISet<string>>;
                    var linksRequiringAuthentication = propertyValue.Item1;
                    
                    if (propertyValue != null)
                    {
                        var authenticationIsRequired = linksRequiringAuthentication[url];

                        if (!user.Identity.IsAuthenticated)
                        {
                            // if not authentication, the link does not get rendered.
                            if (authenticationIsRequired)
                                return MvcHtmlString.Empty;
                        }
                        else
                        {
                            // Some links will only be visible if the User has an Role claim of "admin".
                            var linksRequiringAdminRole = propertyValue.Item2;

                            if (linksRequiringAdminRole.Contains(url))
                            {
                                //var claims = ((ClaimsPrincipal) user).Claims.ToList();
                                if (!user.HasClaim("http://schemas.surgeons.or/2016/06/identity/claims/bqausertype","admin"))
                                {
                                    return MvcHtmlString.Empty;
                                }
                            }
                        }
                    }
                }
            }

            anchorTagBuilder.MergeAttribute(UiConstants.HrefAttribute, url);

            return MvcHtmlString.Create(anchorTagBuilder.ToString());
        }

        public static IHtmlString BootstrapIconLinkButton(
          this HtmlHelper htmlHelper,
          string innerHtml,
          string action,
          string controller,
          string cssClass,
          string font,
          string name,
          string title,
          bool isFormNoValidate,
          bool isAutoFocus,
          string area = null,
          object htmlAttributes = null)
        {
            var anchorTagBuilder = new TagBuilder(UiConstants.Anchor);
            TagBuilder iconTagBuilder = null;

            if (!string.IsNullOrWhiteSpace(font))
            {
                iconTagBuilder = new TagBuilder(UiConstants.I);
                iconTagBuilder.AddCssClass(font);
            }

            HtmlExtensionsCommon.ProcessBootstrapButtonType(cssClass);

            anchorTagBuilder.AddCssClass(cssClass);

            anchorTagBuilder.AddCssClass("btn");

            if (!string.IsNullOrWhiteSpace(title))
            {
                anchorTagBuilder.MergeAttribute(UiConstants.TitleAttribute, title);
            }

            if (isFormNoValidate)
            {
                anchorTagBuilder.MergeAttribute(UiConstants.FormNoValidateAttribute, UiConstants.FormNoValidateAttribute);
            }

            if (isAutoFocus)
            {
                anchorTagBuilder.MergeAttribute(UiConstants.AutoFocusAttribute, UiConstants.AutoFocusAttribute);
            }

            anchorTagBuilder.MergeAttributes(HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));

            HtmlExtensionsCommon.AddName(anchorTagBuilder, name, string.Empty);

            anchorTagBuilder.InnerHtml = iconTagBuilder == null
                ? innerHtml
                : string.Concat(iconTagBuilder.ToString(TagRenderMode.Normal), UiConstants.NonBreakingSpace, innerHtml);

            var httpContext = htmlHelper.ViewContext.HttpContext;

            var routeData = RouteTable.Routes.GetRouteData(httpContext);
            var urlHelper = new UrlHelper(new RequestContext(httpContext, routeData));

            string url = string.Empty;

            // area may be an empty string. This will be root level.
            if(area == null)
                url = urlHelper.Action(action, controller);
            else
                url = urlHelper.Action(action, controller, new { area = area });

            anchorTagBuilder.MergeAttribute(UiConstants.HrefAttribute, url);

            return MvcHtmlString.Create(anchorTagBuilder.ToString());
        }
    }
}
