﻿namespace Adnug.Arch.Mvc.Infrastructure.Logging
{
    public interface ILogger
    {
        void Log(LogEntry entry);
    }
}
