﻿using System;

namespace Adnug.Arch.Mvc.Infrastructure.Logging
{
    public class LogEntry
    {
        public LoggingEventType Severity { get; set; }
        public string Message { get; set; }
        public Exception Exception { get; set; }
    }
}