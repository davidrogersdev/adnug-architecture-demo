﻿namespace Adnug.Arch.Mvc.Infrastructure.Logging
{
    public enum LoggingEventType
    {
        Debug = 0,
        Information = 1,
        Warning = 2,
        Error = 4,
        Fatal = 8
    };
}