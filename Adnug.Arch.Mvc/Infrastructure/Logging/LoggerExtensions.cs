﻿using System;

namespace Adnug.Arch.Mvc.Infrastructure.Logging
{
    public static class LoggerExtensions
    {
        public static void Log(this ILogger logger, string message)
        {
            logger.Log(new LogEntry { Severity = LoggingEventType.Information, Message = message} );
        }

        public static void LogException(this ILogger logger, Exception exception, string message = null)
        {
            logger.Log(string.IsNullOrWhiteSpace(message)
                ? new LogEntry { Severity = LoggingEventType.Error, Message = exception.Message, Exception = exception }
                : new LogEntry { Severity = LoggingEventType.Error, Message = message, Exception = exception }
                );
        }

        public static void LogWarning(this ILogger logger, string message)
        {
            logger.Log(new LogEntry {Severity = LoggingEventType.Warning, Message = message});
        }

        public static void LogError(this ILogger logger, string message)
        {
            logger.Log(new LogEntry {Severity = LoggingEventType.Error, Message = message});
        }

        public static void LogInfo(this ILogger logger, string message)
        {
            logger.Log(new LogEntry {Severity = LoggingEventType.Information, Message = message});
        }

        // More methods here.

    }
}
