﻿using System;
using System.Collections.Generic;

namespace Adnug.Arch.Mvc.Infrastructure.ModelMetadata.Filters
{
	public class WatermarkConventionFilter : IModelMetadataFilter
	{
		public void TransformMetadata(System.Web.Mvc.ModelMetadata metadata,
			IEnumerable<Attribute> attributes)
		{
			if (!string.IsNullOrEmpty(metadata.DisplayName) &&
				string.IsNullOrEmpty(metadata.Watermark))
			{
				metadata.Watermark = string.Concat("Enter ", metadata.DisplayName);
			}
		}
	}
}