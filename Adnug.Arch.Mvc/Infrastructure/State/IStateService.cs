﻿namespace Adnug.Arch.Mvc.Infrastructure.State
{
    public interface IStateService
    {
        void SetValue<T>(string key, T value);
        T GetValue<T>(string key);
        void ClearValue(string key);
        bool HasValue(string key);
    }
}
