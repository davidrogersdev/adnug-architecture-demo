﻿using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace Adnug.Arch.Mvc.Infrastructure.State
{
    public class SessionStateService : IStateService
    {
        private HttpSessionState SessionState => HttpContext.Current.Session;

        public void SetValue<T>(string key, T value)
        {
            if (!ReferenceEquals(SessionState, null))
                SessionState.Add(key, value);
        }

        public void ClearValue(string key)
        {
            SessionState.Remove(key);
        }

        public T GetValue<T>(string key)
        {
            return (T)SessionState[key];
        }

        public bool HasValue(string key)
        {
            return SessionState != null && SessionState.Keys.Cast<string>().Any(k => key == k);
        }
    }
}
