﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Adnug.Arch.Mvc.Infrastructure.Errors
{
    public class ErrorResponseCommand : IErrorResponseCommand
    {
        public void Execute(ErrorResponse errorResponse)
        {
            errorResponse.HttpContext.ClearError();
            errorResponse.HttpContext.Response.Clear();

            if (!ReferenceEquals(errorResponse.ExceptionInstance, null))
            {
                var originalRouteData = RouteTable.Routes.GetRouteData(errorResponse.HttpContext);

                if (!ReferenceEquals(originalRouteData, null))
                {
                    string controllerNameOfOriginalRequest = GetRoutePart(UiConstants.Controller, originalRouteData);

                    string actionNameOfOriginalRequest = GetRoutePart(UiConstants.Action, originalRouteData);

                    //errorResponse.Controller.ViewBag.UncaughtException =
                    //    new HandleErrorInfo(errorResponse.ExceptionInstance, controllerNameOfOriginalRequest, actionNameOfOriginalRequest);
                    errorResponse.NewrouteData.Values["exception"] = new HandleErrorInfo(errorResponse.ExceptionInstance, controllerNameOfOriginalRequest, actionNameOfOriginalRequest);
                }
            }

            ((IController)errorResponse.Controller).Execute(
                new RequestContext(
                    errorResponse.HttpContext,
                    errorResponse.NewrouteData
                    ));
        }

        private static string GetRoutePart(string routePart, RouteData routeData)
        {
            var value = routeData.Values[routePart];

            if (!ReferenceEquals(value, null))
                return value.ToString();

            return string.Empty;
        }
    }
}
