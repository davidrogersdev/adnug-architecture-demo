﻿namespace Adnug.Arch.Mvc.Infrastructure.Errors
{
    public interface IErrorResponseCommand
    {
        void Execute(ErrorResponse errorResponse);
    }
}
