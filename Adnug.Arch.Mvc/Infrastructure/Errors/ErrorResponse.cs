﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Adnug.Arch.Mvc.Infrastructure.Errors
{
    public class ErrorResponse
    {
        public Controller Controller { get; set; }
        public Exception ExceptionInstance { get; set; }
        public HttpContextBase HttpContext { get; set; }
        public RouteData NewrouteData { get; set; }
    }
}