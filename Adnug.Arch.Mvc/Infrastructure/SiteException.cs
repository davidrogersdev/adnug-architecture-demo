﻿using System;

namespace Adnug.Arch.Mvc.Infrastructure
{
    /// <summary>
    /// This class is really just to be used where we want to expose a custom message which can be displyed to the user, rather than the actual
    /// message of a thrown exception.
    /// </summary>
    public class SiteException : Exception
    {
        public SiteException(string message)
            : base(message)
        {
            
        }
    }
}
