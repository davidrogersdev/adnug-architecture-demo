﻿namespace Adnug.Arch.Mvc.Infrastructure
{
    public sealed class UiConstants
    {
        public const string Action = "action";
        public const string Controller = "controller";
        public const string HtmlMimeType = "text/html; charset=utf-8";
        public const string JsonMimeType = "application/json";


        //  HTML Related
        public const string AltAttribute = "alt";
        public const string Anchor = "a";
        public const string AutoFocusAttribute = "autofocus";
        public const string Button = "button";
        public const string Div = "div";
        public const string FormNoValidateAttribute = "formnovalidate";
        public const string HrefAttribute = "href";
        public const string I = "i";
        public const string Image = "image";
        public const string ImageAttribute = "img";
        public const string Input = "input";
        public const string JavascriptTypeText = "text/javascript";
        public const string NonBreakingSpace = "&nbsp;";
        public const string SourceAttribute = "src";
        public const string Script = "script";
        public const string Span = "span";
        public const string ScriptsDirectory = "~/Scripts/";
        public const string Submit = "submit";
        public const string TitleAttribute = "title";
        public const string TypeAttribute = "type";

        //  Views
        public const string BadRequest = "BadRequest";
        public const string PageNotFound = "PageNotFound";
        public const string ServerError = "ServerError";
        public const string StaticContent = "StaticContent";

        // other
        public const string AccountCreated = "AccountCreated";
        public const string FormUrlEncoded = "application/x-www-form-urlencoded";
        public const string ReturnUrl = "returnUrl";
        public const string Success = "success";
        public const string ChangeRequestSuccess = "ChangeRequestSuccess";
        public const string Created = "created";
        public const string Deleted = "deleted";
        public const string Error = "_Error";
        public const string Delimiter = "_|_";
    }
}
