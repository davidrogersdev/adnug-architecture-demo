﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace Adnug.Arch.Mvc.Infrastructure
{
    public static class EnumerableExtensions
    {
        //public static SelectList ToSelectList<T>(this IEnumerable<T> list, object selectedValue = null)
        //    where T : INamedEntity
        //{
        //    return new SelectList(list ?? new List<T>(), "Id", "Name", selectedValue);
        //}

        //public static SelectList ToSelectList<T>(this IEnumerable<T> list, T lookupEntity, object selectedValue = null)
        //   where T : class, ILookupEntity
        //{
        //    return new SelectList((list ?? new List<T>()).Where(t => t.IsActive || t == lookupEntity), "Id", "Name", selectedValue);
        //}

        public static SelectList ToSelectList<TKey, TValue>(this IEnumerable<KeyValuePair<TKey, TValue>> list, object selectedValue = null)
        {
            return new SelectList(list, "Key", "Value");
        }

        public static SelectList ToSelectList<T, T2>(this IDictionary<T, T2> list, object selectedValue = null)
        {
            return new SelectList(list ?? new Dictionary<T, T2>(), "Key", "Value", selectedValue);
        }
    }
}
