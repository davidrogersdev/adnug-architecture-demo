﻿using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Adnug.Arch.Mvc.ActionResults;
using Newtonsoft.Json;

namespace Adnug.Arch.Mvc.Infrastructure
{
    public static class GeneralExtensions
    {
        public static void ToTrace(this ModelStateDictionary instance)
        {
            var errors = instance.Values
                .SelectMany(v => v.Errors)
                .Select(v => new { msg = v.ErrorMessage, exception = v.Exception })
                .ToList();

            errors.ForEach(error => Trace.WriteLine($"Error message: {error.msg}, Exception Message: {error.exception.Message}."));
        }

        public static JsonNetResult ToJsonNetResult(this ModelStateDictionary instance)
        {
            var result = new JsonNetResult
            {
                Data = instance,
            };

            return result;
        }
    }
}
