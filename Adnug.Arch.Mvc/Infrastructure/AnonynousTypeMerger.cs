﻿using System.Collections.Generic;
using System.Dynamic;
using System.Reflection;

namespace Adnug.Arch.Mvc.Infrastructure
{
    public class AnonynousTypeMerger
    {
        /// <summary>
        /// Merges 2 Anonymous typed objects together.
        /// Taken from StackOverflow answer http://stackoverflow.com/a/6802728/540156
        /// </summary>
        /// <param name="item1"></param>
        /// <param name="item2"></param>
        /// <returns></returns>
        public static dynamic Merge(object item1, object item2)
        {
            if (item1 == null || item2 == null)
                return item1 ?? item2 ?? new ExpandoObject();

            dynamic expando = new ExpandoObject();
            var result = (IDictionary<string, object>) expando;

            var item1PropertiesArray = item1.GetType().GetProperties();
            var item2PropertiesArray = item2.GetType().GetProperties();

            for (int index = 0; index < item1PropertiesArray.Length; index++)
            {
                PropertyInfo fi = item1PropertiesArray[index];
                result[fi.Name] = fi.GetValue(item1, null);
            }

            for (int index = 0; index < item2PropertiesArray.Length; index++)
            {
                PropertyInfo fi = item2PropertiesArray[index];
                result[fi.Name] = fi.GetValue(item2, null);
            }

            return result;
        }
    }
}
