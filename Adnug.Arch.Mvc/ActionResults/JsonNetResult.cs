﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using Adnug.Arch.Mvc.Infrastructure;

namespace Adnug.Arch.Mvc.ActionResults
{
    public class JsonNetResult : ActionResult
    {
        public IList<string> ErrorMessages { get; private set; }
        public Encoding ContentEncoding { get; set; }
        public string ContentType { get; set; }
        public object Data { get; set; }

        public JsonRequestBehavior JsonRequestBehavior { get; set; }
        public JsonSerializerSettings SerializerSettings { get; set; }
        public Formatting Formatting { get; set; }
        public DateTimeZoneHandling DateTimeZoneHandling
        {
            set { SerializerSettings.DateTimeZoneHandling = value; }
        }

        public JsonNetResult()
        {
            ErrorMessages = new List<string>();
            SerializerSettings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                Converters = new JsonConverter[]
                {
                    new StringEnumConverter() // string representation of enum, not number
                },
                DateTimeZoneHandling = DateTimeZoneHandling.Utc,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            };
            Formatting = Formatting.None;
            JsonRequestBehavior = JsonRequestBehavior.DenyGet;
            ContentEncoding = Encoding.UTF8;
        }

        public void AddError(string errorMessage)
        {
            ErrorMessages.Add(errorMessage);
        }

        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));

            if (JsonRequestBehavior == JsonRequestBehavior.DenyGet && 
                string.Equals(context.HttpContext.Request.HttpMethod, WebRequestMethods.Http.Get, StringComparison.OrdinalIgnoreCase))
            {
                throw new InvalidOperationException("GET access is not allowed.  Change the JsonRequestBehavior if you need GET access.");
            }

            HttpResponseBase response = context.HttpContext.Response;

            response.ContentType = !string.IsNullOrEmpty(ContentType)
              ? ContentType
              : UiConstants.JsonMimeType;

            response.CacheControl = "no-cache";

            if (ContentEncoding != null)
                response.ContentEncoding = ContentEncoding;

            if (ErrorMessages.Any())
            {
                var originalData = Data;

                Data = new
                {
                    Success = false,
                    OriginalData = originalData,
                    ErrorMessage = string.Join(Environment.NewLine, ErrorMessages),
                    ErrorMessages = ErrorMessages.ToArray()
                };

                // If already 400 or over, this means we have set it to some kind of error already.
                if (response.StatusCode < (int)HttpStatusCode.BadRequest)
                    response.StatusCode = (int)HttpStatusCode.BadRequest;
            }

            if (Data != null)
            {
                var writer = new JsonTextWriter(response.Output) { Formatting = Formatting };

                var serializer = JsonSerializer.Create(SerializerSettings);
                serializer.Serialize(writer, Data);

                writer.Flush();
            }
        }
    }

    public class JsonNetResult<T> : JsonNetResult
    {
        public new T Data
        {
            get { return (T)base.Data; }
            set { base.Data = value; }
        }
    }
}