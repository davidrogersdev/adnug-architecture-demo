﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Adnug.Arch.Mvc.ActionFilters
{
    /// <summary>
    /// Taken from http://timgthomas.com/2013/09/simplify-client-side-validation-by-adding-a-server/
    /// </summary>
    public class ValidatorApiActionFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (actionContext.ModelState.IsValid) return;

            if (actionContext.Request.Method.Method.Equals(WebRequestMethods.Http.Get, StringComparison.OrdinalIgnoreCase))
            {
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.BadRequest);
            }
            else
            {
                var responseMessage = actionContext.Request.CreateResponse(
                    HttpStatusCode.BadRequest,
                    actionContext.ModelState
                    );
                
                actionContext.Response = responseMessage;
            }

        }
    }
}
