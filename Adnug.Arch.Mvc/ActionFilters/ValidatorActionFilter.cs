﻿using System;
using System.Net;
using System.Web.Mvc;
using Adnug.Arch.Mvc.Infrastructure;

namespace Adnug.Arch.Mvc.ActionFilters
{
    /// <summary>
    /// http://timgthomas.com/2013/09/simplify-client-side-validation-by-adding-a-server/
    /// </summary>
    public class ValidatorActionFilter : IActionFilter
    {
        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            // Continue normally if the model is valid.
            if (filterContext.Controller.ViewData.ModelState.IsValid) return;

            if (filterContext.HttpContext.Request.HttpMethod.Equals(WebRequestMethods.Http.Get, StringComparison.OrdinalIgnoreCase))
            {
                var result = new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                filterContext.Result = result;
            }
            else {

                filterContext.HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                filterContext.Result = filterContext.Controller.ViewData.ModelState.ToJsonNetResult();
            }
        }

        public void OnActionExecuted(ActionExecutedContext filterContext)
        {

        }
    }
}