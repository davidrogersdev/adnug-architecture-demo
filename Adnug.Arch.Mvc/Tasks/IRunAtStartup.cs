﻿namespace Adnug.Arch.Mvc.Tasks
{
    public interface IRunAtStartup
    {
        void Execute();
    }
}
