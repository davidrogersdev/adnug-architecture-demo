﻿namespace Adnug.Arch.Mvc.Tasks
{
    public interface IRunOnEachRequest
    {
        void Execute();
    }
}
