﻿namespace Adnug.Arch.Mvc.Tasks
{
    public interface IRunAtInit
    {
        void Execute();
    }
}
