﻿namespace Adnug.Arch.Mvc.Tasks
{
    public interface IRunOnError
    {
        void Execute();
    }
}
