﻿namespace Adnug.Arch.Mvc.Tasks
{
    public interface IRunAfterEachRequest
    {
        void Execute();
    }
}
