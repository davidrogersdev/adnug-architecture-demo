﻿using System;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Web.Mvc;
using Adnug.Arch.Mvc.ActionResults;
using Adnug.Arch.Mvc.Infrastructure;
using Adnug.Arch.Mvc.Infrastructure.Logging;
using FluentValidation;
using MediatR;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace Adnug.Arch.Mvc.Controllers
{
    public class SiteController : Controller
    {
        protected readonly ILogger Logger;
        protected readonly IMediator Mediator;
        protected string Errors;

        public SiteController(ILogger logger, IMediator mediator)
        {
            Logger = logger;
            Mediator = mediator;
        }

        public bool Execute<T>(Func<T> messageFunction)
        {
            try
            {
                messageFunction();

                return true;
            }
            catch (ValidationException exception)
            {
                Errors = string.Join(Environment.NewLine, exception.Errors.Select(e => e.ErrorMessage));
                Logger.LogException(exception, "ValidationException caught in SiteController");
            }
            catch (SiteException exception)
            {
                Errors = exception.Message;
                Logger.LogException(exception);
            }
            catch (DbEntityValidationException dbEntityValidationException)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = dbEntityValidationException.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(dbEntityValidationException.Message, " The validation errors are: ", fullErrorMessage);

                Logger.LogError(exceptionMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, dbEntityValidationException.EntityValidationErrors);                
            }
            catch (Exception exception)
            {
                Errors = "An error has occurred.";
                Logger.LogException(exception, "Exception caught in SiteController.");
            }

            // used to indicate that any transaction which may be in progress needs to be rolled back for this request.
            HttpContext.Items[UiConstants.Error] = true;

            Response.StatusCode = (int)HttpStatusCode.InternalServerError; // fail

            return false;
        }

        protected bool Authorize(string action, string resource, params Claim[] resources)
        {
            // TODO: Here is where you would check the request for Authorization. Ensure the User is authorized.
            //if (ClaimsAuthorization.CheckAccess(action, resource, resources))
            //    return true;

            return true; // Just return true for demo purposes 👍

            Errors = "You are not authorised to access this resource.";
            Response.StatusCode = (int)HttpStatusCode.Forbidden;
            return false;
        }

        protected virtual JsonNetResult JsonNet<T>(T payload,
            Formatting formatting = Formatting.None,
            JsonRequestBehavior jsonRequestBehavior = JsonRequestBehavior.DenyGet,
            JsonSerializerSettings serializerSettings = null,
            Encoding encoding = null,
            string contentType = null)
        {
            var result = new JsonNetResult<T> { Data = payload, JsonRequestBehavior = jsonRequestBehavior, Formatting = formatting };

            if (serializerSettings != null)
                result.SerializerSettings = serializerSettings;

            if (encoding != null)
                result.ContentEncoding = encoding;
            if (contentType != null)
                result.ContentType = contentType;

            return result;
        }

        [Obsolete("Do not use the standard Json helpers to return JSON data to the client.  Use one of the Json[x] methods in the solution instead.")]
        protected JsonResult Json<T>(T data)
        {
            throw new InvalidOperationException("Do not use the standard Json helpers to return JSON data to the client.  Use either JsonSuccess or JsonError instead.");
        }

        protected virtual JsonNetResult JsonError(string errorMessage, JsonRequestBehavior jsonRequestBehavior = JsonRequestBehavior.DenyGet)
        {
            var result = JsonNet(errorMessage);

            result.JsonRequestBehavior = jsonRequestBehavior;

            result.AddError(errorMessage);

            return result;
        }

        protected virtual JsonNetResult JsonSuccess<T>(T data, JsonRequestBehavior jsonRequestBehavior = JsonRequestBehavior.DenyGet)
        {
            var result = JsonNet<T>(data);

            result.JsonRequestBehavior = jsonRequestBehavior;

            return result;
        }


        /******************************************* Methods for Development Purposes Only *******************************************/
        // For debugging only. Tracing will need to be turned on. Delete all calls to this for production.
        protected void DumpModelStateToTrace()
        {
            ModelState.ToTrace();
        }

        protected void Sleep(int? milliSeconds)
        {
            Thread.Sleep(milliSeconds ?? 2000);
        }
        /******************************************* Methods for Development Purposes Only *******************************************/
    }
}
 