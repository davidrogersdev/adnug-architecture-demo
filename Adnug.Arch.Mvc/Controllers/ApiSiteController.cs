﻿using System.Net.Http;
using System;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading;
using System.Web;
using System.Web.Http;
using Adnug.Arch.Mvc.Infrastructure;
using Adnug.Arch.Mvc.Infrastructure.Logging;
using MediatR;

namespace Adnug.Arch.Mvc.Controllers
{
    public class ApiSiteController : ApiController
    {
        protected readonly ILogger _logger;
        protected readonly IMediator _mediator;
        protected string Errors;

        public ApiSiteController(ILogger logger, IMediator mediator)
        {
            _logger = logger;
            _mediator = mediator;
        }

        public bool Execute<T>(Func<T> messageFunction)
        {
            try
            {
                messageFunction();
                
                return true;
            }
            catch (FluentValidation.ValidationException exception)
            {
                Errors = string.Join(Environment.NewLine, exception.Errors.Select(e => e.ErrorMessage));
                _logger.LogException(exception, "ValidationException caught in ApiSiteController");
            }
            catch (SiteException exception)
            {
                Errors = exception.Message;
                _logger.LogException(exception);
            }
            catch (DbEntityValidationException dbEntityValidationException)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = dbEntityValidationException.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(dbEntityValidationException.Message, " The validation errors are: ", fullErrorMessage);

                _logger.LogError(exceptionMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, dbEntityValidationException.EntityValidationErrors);
            }
            catch (Exception exception)
            {
                Errors = "An error has occurred.";
                _logger.LogException(exception, "Exception caught in ApiSiteController.");
            }
            
            return false;
        }

        protected bool Authorize(string action, string resource, params Claim[] resources)
        {
            // TODO: Here is where you would check the request for Authorization. Ensure the User is authorized.
            //if (ClaimsAuthorization.CheckAccess(action, resource, resources))
            //    return true;

            return true; // Just return true for demo purposes 👍

            Errors = "You are not authorised to access this resource.";
            HttpContext.Current.Response.StatusCode = (int)HttpStatusCode.Forbidden;
            return false;
        }
        
        protected virtual IHttpActionResult JsonError(object data)
        {
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.InternalServerError, data));
        }

        protected virtual IHttpActionResult JsonSuccess(object data)
        {
            return Ok(data);
        }


        /******************************************* Methods for Development Purposes Only *******************************************/
        // For debugging only. Tracing will need to be turned on. Delete all calls to this for production.
        protected void Sleep(int? milliSeconds)
        {
            Thread.Sleep(milliSeconds ?? 2000);
        }
        /******************************************* Methods for Development Purposes Only *******************************************/
    }
}
