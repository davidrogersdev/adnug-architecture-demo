﻿using System.Text.RegularExpressions;
using Adnug.Arch.Models;
using FluentValidation;

namespace Adnug.Arch.Validation.Models
{
    public class SimpleFormModelValidator : AbstractValidator<SimpleFormModel>
    {
        public SimpleFormModelValidator()
        {
	        RuleFor(m => m.FirstName)
                .NotEmpty()
                .WithMessage("required:First Name is required.")
                .Length(3, 3)
		        .WithMessage("minlength:First Name must have at least 3 characters.")
	            .Matches(s => new Regex("^\\w+x$"))
		        .WithMessage("endInXRule:First Name end with the letter x.");
        }
	}
}