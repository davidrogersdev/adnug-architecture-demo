﻿using Adnug.Arch.Models.ViewModels;
using FluentValidation;

namespace Adnug.Arch.Validation.Models.ViewModels
{
    public class LicenceEditModelValidator : AbstractValidator<LicenceEditModel>
    {
        public LicenceEditModelValidator()
        {
            RuleFor(m => m.Licence.LicenceKey)
                .NotEmpty()
                .WithMessage("required:LicenceKey is a required field.");

            RuleFor(m => m.Licence.Software.NameAndDescription)
                .NotEmpty()
                .WithMessage("required:The Name and Description field is required.")
                .Must(NameAndDescriptionMustHaveColon)
                .WithMessage("adHasColon:The Name and Description field must have a colon.");
        }

        private bool NameAndDescriptionMustHaveColon(LicenceEditModel model, string nameAndDescription)
        {
            // This is not what we are validating, so return true. Purely to avoid a null ref exception.
            // NotEmpty (above ⬆️) will validate whether the fields has a value or not
            if (string.IsNullOrWhiteSpace(nameAndDescription))
                return true; 

            return nameAndDescription.Contains(":");
        }
    }
}