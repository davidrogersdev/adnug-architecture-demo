﻿using System;
using System.Linq;
using Adnug.Arch.DataAccess.Db;
using Adnug.Arch.Messaging.Commands;
using Adnug.Arch.Mvc.Infrastructure;
using FluentValidation;

namespace Adnug.Arch.Validation.Commands
{
    public class UpdateLicenceCommandValidator : AbstractValidator<UpdateLicenceCommand>
    {
        private readonly LicenceTrackerContext _licenceTrackerContext;

        public UpdateLicenceCommandValidator(IDbContextFoundary dbContextFoundary)
        {
            _licenceTrackerContext = dbContextFoundary.GetCurrentContext();

            RuleFor(m => m.LicenceEditModel.Licence.LicenceKey)
                .Must(NotBeAnExistingLicenceKey)
                .WithMessage("{0}: You cannot use an existing licence key of another Licence.{1}", "LicenceKey", UiConstants.Delimiter);
        }

        private bool NotBeAnExistingLicenceKey(UpdateLicenceCommand model, string licenceKey)
        {
            var licence = _licenceTrackerContext.Licences.Single(l => l.Id == model.LicenceEditModel.Licence.Id);

            if(licence.LicenceKey.Equals(licenceKey, StringComparison.Ordinal))
                return true;

            var licenceKeys = _licenceTrackerContext.Licences.Select(l => l.LicenceKey);

            return !licenceKeys.Contains(licenceKey);
        }
    }
}