﻿using System;
using System.Linq;
using System.Reflection;
using FluentValidation;
using SimpleInjector;

namespace Adnug.Arch.Infrastructure.Ioc
{
    public static class ValidationRegistry
    {
        public static void RegisterFluentValidation(this Container container, Assembly assembly, Assembly servicesAssembly)
        {
            /******************************************* Web Assembly *******************************************/
            //  Auto-Register all the validators which are stored in the Web assembly.
            container.Register(typeof(IValidator<>), new[] { assembly }, Lifestyle.Transient);
            container.RegisterCollection(typeof(IValidator<>), new[] { assembly });


            /******************************************* Core Assembly *******************************************/

            // 💢 IMPORTANT: we have to register validators in the services assembly differently, because they have a different lifetsyle.
            // You can only use container.RegisterCollection on types where they all have the same lifestyle. Other types must be 
            // registered 'more directly'.

            //  Auto-Register all the validators which are stored in the servicesAssembly assembly.
            var inServiceValidators = servicesAssembly.GetExportedTypes()
                .Where(t => t.Namespace != null 
                            && t.Namespace.StartsWith(string.Concat(servicesAssembly.GetName().Name, ".Validation"), StringComparison.Ordinal))
                .Where(t => t.GetInterfaces().Any())
                .Select(type => new
                {
                    Service = type.GetInterfaces().First(),
                    Implementation = type
                });

            foreach (var applicationService in inServiceValidators)
            {
                container.Register(applicationService.Service, applicationService.Implementation, Lifestyle.Singleton);
            }
        }
    }
}