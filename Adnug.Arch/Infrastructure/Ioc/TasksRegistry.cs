﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Adnug.Arch.Mvc.Tasks;
using SimpleInjector;

namespace Adnug.Arch.Infrastructure.Ioc
{
    public static class TasksRegistry
    {
        public static void RegisterTasks(this Container container, Assembly webAssembly, Assembly mvcAssembly)
        {
            var assembliesToScan = new[] { webAssembly, mvcAssembly };

            var interfaceIRunOnEachRequest = typeof(IRunOnEachRequest);
            var interfaceIRunAfterEachRequest = typeof(IRunAfterEachRequest);
            var interfaceIRunOnError = typeof(IRunOnError);
            var interfaceIRunAtInit = typeof(IRunAtInit);
            var interfaceIRunAtStartup = typeof(IRunAtStartup);

            var registrationsRunOnEachRequest = GetTypesForRegistration(assembliesToScan, interfaceIRunOnEachRequest);
            var registrationsRunAfterEachRequest = GetTypesForRegistration(assembliesToScan, interfaceIRunAfterEachRequest);
            var registrationsRunOnError = GetTypesForRegistration(assembliesToScan, interfaceIRunOnError);
            var registrationsRunAtInit = GetTypesForRegistration(assembliesToScan, interfaceIRunAtInit);
            var registrationsRunAtStartup = GetTypesForRegistration(assembliesToScan, interfaceIRunAtStartup);

            container.RegisterCollection<IRunOnEachRequest>(registrationsRunOnEachRequest);
            container.RegisterCollection<IRunAfterEachRequest>(registrationsRunAfterEachRequest);
            container.RegisterCollection<IRunOnError>(registrationsRunOnError);
            container.RegisterCollection<IRunAtInit>(registrationsRunAtInit);
            container.RegisterCollection<IRunAtStartup>(registrationsRunAtStartup);
        }

        private static IEnumerable<Type> GetTypesForRegistration(IEnumerable<Assembly> assembliesToScan, Type interfaceTypeToRegister)
        {
            var registrationsRunOnEachRequest = assembliesToScan
                .SelectMany(s => s.GetTypes())
                .Where(interfaceTypeToRegister.IsAssignableFrom)
                .Where(p => p.GetInterfaces().Any());

            return registrationsRunOnEachRequest;
        }

    }
}