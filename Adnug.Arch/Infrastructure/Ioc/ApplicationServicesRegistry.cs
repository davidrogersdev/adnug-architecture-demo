﻿using System;
using System.Linq;
using System.Reflection;
using SimpleInjector;

namespace Adnug.Arch.Infrastructure.Ioc
{
    public static class ApplicationServicesRegistry
    {
        public static void RegisterApplicationServices(this Container container, Assembly assembly)
        {
            var applicationServices = assembly.GetExportedTypes()
                .Where(t => t.Namespace != null 
                            && t.Namespace.StartsWith(string.Concat(assembly.GetName().Name, ".ApplicationServices"), StringComparison.Ordinal))
                .Where(t => t.GetInterfaces().Any())
                .Select(type => new
                {
                    Service = type.GetInterfaces().Single(),
                    Implementation = type
                });

            foreach (var applicationService in applicationServices)
            {
                container.Register(applicationService.Service, applicationService.Implementation, Lifestyle.Singleton);
            }
        }
    }
}