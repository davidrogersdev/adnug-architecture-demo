﻿using System;
using System.Reflection;
using System.Security.Principal;
using System.Web;
using Adnug.Arch.Infrastructure.Identity;
using Adnug.Arch.Mvc.Infrastructure.Errors;
using SimpleInjector;

namespace Adnug.Arch.Infrastructure.Ioc
{
    public static class MvcRegistry
    {
        public static void RegisterMvcParts(this Container container, Assembly mvcAssembly)
        {
            if (container == null) throw new ArgumentNullException(nameof(container));
            if (mvcAssembly == null) throw new ArgumentNullException(nameof(mvcAssembly));

            container.Register<IPrincipal>(() => HttpContext.Current.User, Lifestyle.Scoped);
            container.Register<IIdentity>(() => HttpContext.Current.User.Identity, Lifestyle.Scoped);
            container.Register<HttpRequestBase>(() => new HttpRequestWrapper(HttpContext.Current.Request), Lifestyle.Scoped);
            container.Register<HttpContextBase>(() => new HttpContextWrapper(HttpContext.Current), Lifestyle.Scoped);
            container.Register<HttpSessionStateBase>(() => HttpContext.Current.Session != null
                ? new HttpSessionStateWrapper(HttpContext.Current.Session)
                : null
                , Lifestyle.Scoped);
            container.Register<HttpServerUtilityBase>(() => new HttpServerUtilityWrapper(HttpContext.Current.Server), Lifestyle.Scoped);


            container.Register<ICurrentUser, CurrentUser>(Lifestyle.Scoped); // TODO: make relevant to whatever ASP Identity class is used. Create implementation class


            // I use this to handle uncaught exceptions to redirect to error pages. Not used in demo.
            container.Register<IErrorResponseCommand, ErrorResponseCommand>(Lifestyle.Transient); 

            /************************************************ Not used in this demo - but serious fancy pants MVC Extensibility stuff. ************************************************/
            //container.Register<ModelMetadataProvider, ExtensibleModelMetadataProvider>(Lifestyle.Transient);

            //var registrations = mvcAssembly
            //    .GetTypes()
            //    .Where(p => typeof(IModelMetadataFilter).IsAssignableFrom(p))
            //    .Where(p => p.GetInterfaces().Any());


            //container.RegisterCollection<IModelMetadataFilter>(registrations);
        }
    }
}