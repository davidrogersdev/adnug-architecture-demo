﻿using Adnug.Arch.DataAccess.Db;
using SimpleInjector;

namespace Adnug.Arch.Infrastructure.Ioc
{
    public static class OrmRegistry
    {
        public static void RegisterEfParts(this Container container)
        {
            container.Register<IDbContextFoundary, DbContextFoundary>(Lifestyle.Singleton);
            container.Register(() => new LicenceTrackerContext(), Lifestyle.Scoped);
        }
    }
}