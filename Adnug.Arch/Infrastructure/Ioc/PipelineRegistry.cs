﻿using System.Reflection;
using Adnug.Arch.Messaging.Decorators;
using MediatR;
using SimpleInjector;

namespace Adnug.Arch.Infrastructure.Ioc
{
    public static class PipelineRegistry
    {
        public static void RegisterPipelineParticipants(this Container container, Assembly assembly)
        {
            container.Register(typeof(IRequestHandler<,>), new[] { assembly }, Lifestyle.Scoped);

            container.Register(typeof(IPipelineBehavior<,>), new[] { assembly }, Lifestyle.Scoped);

            // Order matters. The last item in the array will be the outer-most layer of the onion.
            container.RegisterCollection(typeof(IPipelineBehavior<,>)
                , new[] { typeof(ValidationPipelineBehavior<,>)
                , typeof(DodgyProfilerPipelineBehavior<,>) }
                );
        }

    }
}