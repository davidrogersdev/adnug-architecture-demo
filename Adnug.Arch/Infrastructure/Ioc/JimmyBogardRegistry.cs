﻿using System;
using System.Linq;
using System.Reflection;
using Adnug.Arch.Messaging.Decorators;
using AutoMapper;
using MediatR;
using SimpleInjector;

namespace Adnug.Arch.Infrastructure.Ioc
{
    public static class JimmyBogardRegistry
    {
        public static void RegisterMediatrParts(this Container container, Assembly assembly)
        {
            container.Register<SingleInstanceFactory>(() => t => container.GetInstance(t), Lifestyle.Scoped);
            container.Register<MultiInstanceFactory>(() => t => container.GetAllInstances(t), Lifestyle.Scoped);
            container.Register<IMediator, Mediator>(Lifestyle.Scoped);

            container.Register(typeof(IRequestHandler<,>), new[] { assembly}, Lifestyle.Scoped);

            // Order matters. Top to bottom, inner to outer decorator.
            container.RegisterDecorator(typeof(IRequestHandler<,>), typeof(PipelineValidationDecorator<,>), Lifestyle.Scoped);
            container.RegisterDecorator(typeof(IRequestHandler<,>), typeof(TransactionCommandHandlerDecorator<,>), Lifestyle.Scoped);
            container.RegisterDecorator(typeof(IRequestHandler<,>), typeof(DeadlockRetryCommandHandlerDecorator<,>), Lifestyle.Scoped);
            container.RegisterDecorator(typeof(IRequestHandler<,>), typeof(DodgyProfilerDecorator<,>), Lifestyle.Scoped);
            container.RegisterDecorator(typeof(IRequestHandler<,>), typeof(LoggingNdcDecorator<,>), Lifestyle.Scoped);
        }

        public static void RegisterAutomapperParts(this Container container, Assembly webAssembly)
        {
            var config = GetMapperConfiguration(webAssembly);

            // Singleton lifetimes should be OK for Automapper 
            container.Register(() => config, Lifestyle.Singleton);
            container.Register(() => container.GetInstance<MapperConfiguration>().CreateMapper(), Lifestyle.Singleton);
        }

        private static MapperConfiguration GetMapperConfiguration(Assembly webAssembly)
        {
            var profiles = webAssembly.GetTypes()
                           .Where(t => typeof(Profile).IsAssignableFrom(t))
                           .Select(t => (Profile)Activator.CreateInstance(t))
                           .ToList();

            var config = new MapperConfiguration(cfg =>
            {
                // ReSharper disable once ForCanBeConvertedToForeach
                for (int i = 0; i < profiles.Count; i++)
                {
                    cfg.AddProfile(profiles[i]);
                }
            });

#if DEBUG
            config.AssertConfigurationIsValid();
#endif
            return config;
        }
    }
}