﻿using Adnug.Arch.Infrastructure.Logging;
using Adnug.Arch.Mvc.Infrastructure.Logging;
using SimpleInjector;

namespace Adnug.Arch.Infrastructure.Ioc
{
    public static class InfrastructureRegistry
    {
        public static void RegisterLoggingServices(this Container container)
        {
            container.RegisterConditional(
                typeof(ILogger),
                c => typeof(Log4NetAdapter<>).MakeGenericType(c.Consumer.ImplementationType),
                Lifestyle.Singleton,
                c => true);
        }
    }
}