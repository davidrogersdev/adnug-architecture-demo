﻿using System;
using Adnug.Arch.Mvc.Infrastructure.Logging;
using log4net;

namespace Adnug.Arch.Infrastructure.Logging
{
    public class Log4NetAdapter<T> : ILogger
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(T));


        public void Log(LogEntry entry)
        {
            //Here invoke logger
            switch (entry.Severity)
            {
                case LoggingEventType.Debug:
                    Logger.Debug(entry.Message, entry.Exception); break;
                case LoggingEventType.Error:
                    Logger.Error(entry.Message, entry.Exception); break;
                case LoggingEventType.Fatal:
                    Logger.Fatal(entry.Message, entry.Exception); break;
                case LoggingEventType.Information:
                    Logger.Info(entry.Message, entry.Exception); break;
                case LoggingEventType.Warning:
                    Logger.Warn(entry.Message, entry.Exception); break;
                default:
                    throw new NotSupportedException($"The severity level {entry.Severity} is not supported.");
            }
        }
    }
}