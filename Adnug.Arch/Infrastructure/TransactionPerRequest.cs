﻿using System.Data;
using System.Data.Entity;
using System.Web;
using Adnug.Arch.DataAccess.Db;
using Adnug.Arch.Mvc.Infrastructure;
using Adnug.Arch.Mvc.Tasks;

namespace Adnug.Arch.Infrastructure
{
    //public class TransactionPerRequest : IRunOnEachRequest, IRunOnError, IRunAfterEachRequest
    //{
    //    private const string Transaction = "_Transaction";
    //    private readonly HttpContextBase _httpContext;
    //    private readonly LicenceTrackerContext _licenceTrackerContext;

    //    public TransactionPerRequest(HttpContextBase httpContext, LicenceTrackerContext licenceTrackerContext)
    //    {
    //        _httpContext = httpContext;
    //        _licenceTrackerContext = licenceTrackerContext;
    //    }

    //    void IRunOnEachRequest.Execute()
    //    {
    //        _httpContext.Items[Transaction] =
    //            _licenceTrackerContext.Database.BeginTransaction(IsolationLevel.ReadCommitted);

    //    }

    //    void IRunOnError.Execute()
    //    {
    //        _httpContext.Items[UiConstants.Error] = true;
    //    }

    //    void IRunAfterEachRequest.Execute()
    //    {
    //        var transaction = (DbContextTransaction)_httpContext.Items[Transaction];

    //        if (_httpContext.Items[UiConstants.Error] == null)
    //        {
    //            transaction.Commit();
    //        }
    //        else
    //        {
    //            transaction.Rollback();
    //        }
    //    }
    //}
}