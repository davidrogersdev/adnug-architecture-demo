﻿using System;
using System.Linq;
using Adnug.Arch.Core.DataTransferObjects;
using Adnug.Arch.Core.Mapping;
using AutoMapper;

namespace Adnug.Arch.Infrastructure.Mapping
{
    public class AdnugArchProfile : Profile
    {
        const string AdnugArchProfileName = "AdnugArchProfile";

        public AdnugArchProfile()
            : base(AdnugArchProfileName)
        {
            ConfigureProfile();

            //  Initialize all mappings here
            InitializeMappings();
        }

        private void InitializeMappings()
        {
            var coreAssemblyTypes = typeof(LicenceDto).Assembly.GetExportedTypes();
            //var webAssemblyTypes = typeof(MvcApplication).Assembly.GetExportedTypes();
            LoadStandardMappings(coreAssemblyTypes);
            LoadCustomMappings(coreAssemblyTypes);
        }

        private void ConfigureProfile()
        {
            SourceMemberNamingConvention = new PascalCaseNamingConvention();
            DestinationMemberNamingConvention = new PascalCaseNamingConvention();

            ShouldMapField = field => false; // do not map fields. Only properties.
        }

        private void LoadStandardMappings(Type[] coreAssemblyTypes)
        {
            var maps = (from t in coreAssemblyTypes
                        from i in t.GetInterfaces()
                        where i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IMapFrom<>) &&
                              !t.IsAbstract &&
                              !t.IsInterface
                        select new
                        {
                            Source = i.GetGenericArguments()[0],
                            Destination = t
                        }).ToArray();

            foreach (var map in maps)
            {
                CreateMap(map.Source, map.Destination);
            }
        }

        private void LoadCustomMappings(Type[] coreAssemblyTypes)
        {
            // For those who prefer the Lambda syntax, I've written this query in that style.
            var maps = coreAssemblyTypes.SelectMany(t => t.GetInterfaces().Select(i => new
                {
                    Type = t,
                    Interface = i
                }))
                .Where(t => t.Interface.IsGenericType && t.Interface.GetGenericTypeDefinition() == typeof(IHaveCustomMappings<>) &&
                            !t.Type.IsAbstract &&
                            !t.Type.IsInterface)
                .Select(t => new
                {
                    CustomMappingType = t.Interface.GetGenericArguments()[0]
                }).ToArray();

            var customMappings = maps.Select(l => (IConfigureThisMapping)Activator.CreateInstance(l.CustomMappingType)).ToArray();

            foreach (var map in customMappings)
            {
                map.CreateMappings(this);
            }
        }
    }
}