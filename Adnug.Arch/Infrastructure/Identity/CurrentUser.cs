using System.Security.Claims;
using System.Security.Principal;
using Thinktecture.IdentityModel.Extensions;

namespace Adnug.Arch.Infrastructure.Identity
{
    public class CurrentUser : ICurrentUser
    {
        private readonly IIdentity _identity;
        private readonly IPrincipal _principal;

        public CurrentUser(IIdentity identity, IPrincipal principal)
        {
            _identity = identity;
            _principal = principal;
        }

        public string FullName => ((ClaimsPrincipal) _principal).FindFirst("full_name").Value.Trim();
        public string UserName => ((ClaimsPrincipal) _principal).FindFirst("username").Value.Trim();

        public bool HasClaim(string claim)
        {
            return ((ClaimsPrincipal)_principal).HasClaim(claim);
        }

        
        public bool IsAuthenticated => _identity.IsAuthenticated;
    }
}