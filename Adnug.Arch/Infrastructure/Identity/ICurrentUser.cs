namespace Adnug.Arch.Infrastructure.Identity
{
    public interface ICurrentUser
    {
        string FullName { get; }
        bool HasClaim(string claim);
        bool IsAuthenticated { get; }
        string UserName { get; }
    }
}