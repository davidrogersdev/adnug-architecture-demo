﻿using System.Linq;
using Adnug.Arch.Core.DataTransferObjects;
using Adnug.Arch.DataAccess.Db;
using Adnug.Arch.Messaging.Queries;
using Adnug.Arch.Models.ViewModels;
using MediatR;

namespace Adnug.Arch.Messaging.QueryHandlers
{
    public class GetLicencesQueryHandler : IRequestHandler<GetLicencesQuery, LicencesViewModel>
    {
        private readonly IDbContextFoundary _dbContextFoundary;

        public GetLicencesQueryHandler(IDbContextFoundary dbContextFoundary)
        {
            _dbContextFoundary = dbContextFoundary;
        }

        public LicencesViewModel Handle(GetLicencesQuery message)
        {
            var context = _dbContextFoundary.GetCurrentContext();
            
            return new LicencesViewModel
            {
                Licences = context.Licences.Select(l => new LicenceDto
                {
                    Id = l.Id,
                    LicenceKey = l.LicenceKey,
                    
                })
            };
        }
    }
}