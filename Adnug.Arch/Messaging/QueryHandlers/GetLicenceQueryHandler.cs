﻿using System;
using Adnug.Arch.Core.ApplicationServices;
using Adnug.Arch.Messaging.Queries;
using Adnug.Arch.Models.ViewModels;
using Adnug.Arch.Mvc.Infrastructure;
using MediatR;

namespace Adnug.Arch.Messaging.QueryHandlers
{
    public class GetLicenceQueryHandler : IRequestHandler<GetLicenceQuery, LicenceEditModel>
    {
        private readonly ILicenceDataService _licenceDataService;

        public GetLicenceQueryHandler(ILicenceDataService licenceDataService)
        {
            _licenceDataService = licenceDataService;
        }

        public LicenceEditModel Handle(GetLicenceQuery message)
        {
            if (message == null) throw new ArgumentNullException(nameof(message));

            var licenceDto = _licenceDataService.GetLicence(message.LicenceId);

            if (licenceDto == null)
                throw new SiteException($"There is no licence with an id of {message.LicenceId}");

            return new LicenceEditModel { Licence = licenceDto };
        }
    }
}