using Adnug.Arch.Models.ViewModels;
using MediatR;

namespace Adnug.Arch.Messaging.Queries
{
    public class GetLicenceQuery : IRequest<LicenceEditModel>
    {
        public int LicenceId { get; set; }
    }
}