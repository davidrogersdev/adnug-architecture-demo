﻿using Adnug.Arch.Models.ViewModels;
using MediatR;

namespace Adnug.Arch.Messaging.Queries
{
    public class GetLicencesQuery : IRequest<LicencesViewModel>
    {
    }
}