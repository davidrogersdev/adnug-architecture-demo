﻿using System;
using Adnug.Arch.Core.ApplicationServices;
using Adnug.Arch.Messaging.Commands;
using MediatR;

namespace Adnug.Arch.Messaging.CommandHandlers
{
    public class UpdateLicenceCommandHandler : IRequestHandler<UpdateLicenceCommand, int>
    {
        private readonly ILicenceDataService _licenceDataService;

        public UpdateLicenceCommandHandler(ILicenceDataService licenceDataService)
        {
            _licenceDataService = licenceDataService;
        }

        public int Handle(UpdateLicenceCommand message)
        {
            if (message == null) throw new ArgumentNullException(nameof(message));

            var licenceDto = message.LicenceEditModel.Licence;

            return _licenceDataService.UpdateLicence(licenceDto);
        }
    }
}