﻿using Adnug.Arch.Models.ViewModels;
using Adnug.Arch.Validation.Commands;
using FluentValidation.Attributes;
using MediatR;

namespace Adnug.Arch.Messaging.Commands
{
    [Validator(typeof(UpdateLicenceCommandValidator))]
    public class UpdateLicenceCommand : IRequest<int>
    {
        public LicenceEditModel LicenceEditModel { get; set; }
    }
}