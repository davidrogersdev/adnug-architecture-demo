﻿namespace Adnug.Arch.Messaging
{
    public sealed class OperationTypes
    {
        public const string Command = "Command";
        public const string Query = "Query";
    }
}