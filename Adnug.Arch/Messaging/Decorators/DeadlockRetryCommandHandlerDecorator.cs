﻿using System;
using System.Data.Common;
using System.Threading;
using MediatR;

namespace Adnug.Arch.Messaging.Decorators
{
    public class DeadlockRetryCommandHandlerDecorator<TRequest, TRepsonse> : IRequestHandler<TRequest, TRepsonse>, IDisposable
        where TRequest : IRequest<TRepsonse>
    {
        private readonly IRequestHandler<TRequest, TRepsonse> _decoratedHandler;

        public DeadlockRetryCommandHandlerDecorator(IRequestHandler<TRequest, TRepsonse> innerHandler)
        {
            _decoratedHandler = innerHandler;
        }

        public TRepsonse Handle(TRequest message)
        {
            if (typeof(TRequest).Name.EndsWith(OperationTypes.Command, StringComparison.Ordinal))
            {
                return HandleWithCountDown(message, 5);
            }

            return _decoratedHandler.Handle(message);
        }

        private TRepsonse HandleWithCountDown(TRequest command, int count)
        {
            try
            {
                return _decoratedHandler.Handle(command);
            }
            catch (Exception ex)
            {
                if (count <= 0 || !IsDeadlockException(ex))
                    throw;

                Thread.Sleep(300);

                return HandleWithCountDown(command, count - 1);
            }
        }

        private static bool IsDeadlockException(Exception exception)
        {
            while (exception != null)
            {
                if (exception is DbException && exception.Message.Contains("deadlock"))
                    return true;

                exception = exception.InnerException;
            }

            return false;
        }

        public void Dispose()
        {
            var disposable = _decoratedHandler as IDisposable;

            disposable?.Dispose();
        }
    }
}