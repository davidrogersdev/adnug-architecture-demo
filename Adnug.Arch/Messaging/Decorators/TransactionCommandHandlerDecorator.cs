﻿using System;
using System.Data;
using Adnug.Arch.DataAccess.Db;
using MediatR;

namespace Adnug.Arch.Messaging.Decorators
{
    public class TransactionCommandHandlerDecorator<TRequest, TRepsonse> : IRequestHandler<TRequest, TRepsonse>, IDisposable
        where TRequest : IRequest<TRepsonse>
    {
        private readonly IRequestHandler<TRequest, TRepsonse> _decoratedHandler;
        private readonly IDbContextFoundary _dbContextFoundary;

        public TransactionCommandHandlerDecorator(IRequestHandler<TRequest, TRepsonse> _decoratedHandler, IDbContextFoundary dbContextFoundary)
        {
            this._decoratedHandler = _decoratedHandler;
            _dbContextFoundary = dbContextFoundary;
        }

        public TRepsonse Handle(TRequest message)
        {
            using (var transaction = _dbContextFoundary.GetCurrentContext().Database.BeginTransaction(IsolationLevel.ReadCommitted))
            {
                try
                {
                    TRepsonse response = _decoratedHandler.Handle(message);

                    transaction.Commit();

                    return response;
                }
                catch (Exception exception)
                {
                    transaction.Rollback();

                    throw;
                }
            }
        }
        
        public void Dispose()
        {
            var disposable = _decoratedHandler as IDisposable;

            disposable?.Dispose();
        }
    }
}