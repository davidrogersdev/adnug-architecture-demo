﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using MediatR;

namespace Adnug.Arch.Messaging.Decorators
{
    public class DodgyProfilerPipelineBehavior<TRequest, TRepsonse> : IPipelineBehavior<TRequest, TRepsonse>, IDisposable
    {
        public DodgyProfilerPipelineBehavior()
        {
            
        }

        public async Task<TRepsonse> Handle(TRequest request, RequestHandlerDelegate<TRepsonse> next)
        {
            var response = await next();

            return response;
        }

        public void Dispose()
        {
            
        }
    }
}