﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentValidation;
using MediatR;

namespace Adnug.Arch.Messaging.Decorators
{
    public class PipelineValidationDecorator<TRequest, TRepsonse> : IRequestHandler<TRequest, TRepsonse>, IDisposable
        where TRequest : IRequest<TRepsonse>
    {
        private readonly IRequestHandler<TRequest, TRepsonse> _decoratedHandler;
        private readonly IEnumerable<IValidator<TRequest>> _validators;

        public PipelineValidationDecorator(IRequestHandler<TRequest, TRepsonse> innerHandler,
            IEnumerable<IValidator<TRequest>> validators)
        {
            _decoratedHandler = innerHandler;
            _validators = validators;
        }

        public TRepsonse Handle(TRequest message)
        {
            var context = new ValidationContext(message);

            var failures = _validators
                        .Select(v => v.Validate(context))
                        .SelectMany(result => result.Errors)
                        .Where(f => f != null)
                        .ToList();

            if (failures.Any())
                throw new ValidationException(failures);

            return _decoratedHandler.Handle(message);
        }

        public void Dispose()
        {
            var disposable = _decoratedHandler as IDisposable;

            disposable?.Dispose();
        }
    }
}