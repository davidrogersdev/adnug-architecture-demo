﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using FluentValidation;
using MediatR;

namespace Adnug.Arch.Messaging.Decorators
{
    public class ValidationPipelineBehavior<TRequest, TRepsonse> : IPipelineBehavior<TRequest, TRepsonse>, IDisposable
    {
        private readonly IEnumerable<IValidator<TRequest>> _validators;

        public ValidationPipelineBehavior(IEnumerable<IValidator<TRequest>> validators)
        {
            _validators = validators;
        }

        public void Dispose()
        {
        }

        public async Task<TRepsonse> Handle(TRequest request, RequestHandlerDelegate<TRepsonse> next)
        {
            var context = new ValidationContext(request);

            var failures = _validators
                        .Select(v => v.Validate(context))
                        .SelectMany(result => result.Errors)
                        .Where(f => f != null)
                        .ToList();

            if (failures.Any())
                throw new ValidationException(failures);

            var response = await next();

            return response;
        }
    }
}