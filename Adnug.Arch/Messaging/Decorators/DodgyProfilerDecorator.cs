﻿using System;
using System.Diagnostics;
using Adnug.Arch.Mvc.Infrastructure.Logging;
using MediatR;

namespace Adnug.Arch.Messaging.Decorators
{
    public class DodgyProfilerDecorator<TRequest, TRepsonse> : IRequestHandler<TRequest, TRepsonse>, IDisposable
        where TRequest : IRequest<TRepsonse>
    {
        private readonly IRequestHandler<TRequest, TRepsonse> _decoratedHandler;
        private readonly ILogger _logger;

        public DodgyProfilerDecorator(IRequestHandler<TRequest, TRepsonse> decoratedHandler, ILogger logger)
        {
            _decoratedHandler = decoratedHandler;
            _logger = logger;
        }

        public TRepsonse Handle(TRequest message)
        {
            var stopwatch = Stopwatch.StartNew();

            var response = _decoratedHandler.Handle(message);

            stopwatch.Stop();

            _logger.LogInfo($"Execution func took {stopwatch.ElapsedMilliseconds} Milliseconds.");

            return response;
        }

        public void Dispose()
        {
            var disposable = _decoratedHandler as IDisposable;

            disposable?.Dispose();
        }
    }
}