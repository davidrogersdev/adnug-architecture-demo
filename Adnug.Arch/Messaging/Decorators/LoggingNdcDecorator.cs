﻿using System;
using log4net;
using MediatR;

namespace Adnug.Arch.Messaging.Decorators
{
    public class LoggingNdcDecorator<TRequest, TRepsonse> : IRequestHandler<TRequest, TRepsonse>, IDisposable
        where TRequest : IRequest<TRepsonse>
    {
        // ReSharper disable once InconsistentNaming
        private const string NDC = "NDC";
        private readonly IRequestHandler<TRequest, TRepsonse> _decoratedHandler;
        private IDisposable _ctx;

        public LoggingNdcDecorator(IRequestHandler<TRequest, TRepsonse> decoratedHandler)
        {
            _decoratedHandler = decoratedHandler;
        }

        public TRepsonse Handle(TRequest message)
        {
            _ctx = ThreadContext.Stacks[NDC].Push(Guid.NewGuid().ToString());
            
            var response = _decoratedHandler.Handle(message);
            
            return response;
        }

        public void Dispose()
        {
            var disposable = _decoratedHandler as IDisposable;

            disposable?.Dispose();

            if (_ctx == null) return;

            ThreadContext.Stacks[NDC].Pop();
            _ctx.Dispose();
        }
    }
}