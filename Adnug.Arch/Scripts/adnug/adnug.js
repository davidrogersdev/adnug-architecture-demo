﻿var adnug = (function () {
    'use strict';

    moment.locale('en-AU');

    var firstLetterCapitalized = function(str) {
        return str.charAt(0).toUpperCase() + str.substring(1).toLowerCase();
    }

    function existy(it) {
        return it != null;
    }

    function removeItem(array, item) {

        var index = array.indexOf(item);

        if (index !== -1) {
            array.splice(index, 1);
        }
    }

    function textContains(text, searchText) {
        return text && -1 !== text.toLowerCase().indexOf(searchText.toLowerCase());
    }

    function concatArrays() {
        var head = _.first(arguments);
        if (existy(head))
            return head.concat.apply(head, _.rest(arguments));
        else
            return [];
    }

    var noNull = function (fun /*, defaults */) {
        var defaults = _.rest(arguments);
        return function ( /* args */) {
            var args = _.map(arguments, function (e, i) {
                return existy(e) ? e : defaults[i];
            });
            return fun.apply(null, args);
        };
    };

    var defaults = function (d) {
        return function (o, k) {
            var val = noNull(_.identity, d[k]);
            return o && val(o[k]);
        };
    };

    // native augmentations
    if (!String.prototype.substringUntil) {
        String.prototype.substringUntil = function (str) {
            return this.slice(0, this.indexOf(str));
        };
    };

    if (!String.prototype.substringAfter) {
        String.prototype.substringAfter = function (str) {
            return this.slice(this.indexOf(str) + 1);
        };
    };

    if (!String.prototype.startsWith) {
        String.prototype.startsWith = function (searchString, position) {
            position = position || 0;
            return this.substr(position, searchString.length) === searchString;
        };
    }

    Number.isFinite = Number.isFinite || function (value) {
        return typeof value === "number" && isFinite(value);
    }
    
    return {
        concatArrays: concatArrays,
        defaults: defaults,
        existy: existy,
        firstLetterCapitalized: firstLetterCapitalized,
        removeItem: removeItem,
        textContains: textContains
    };
}());

