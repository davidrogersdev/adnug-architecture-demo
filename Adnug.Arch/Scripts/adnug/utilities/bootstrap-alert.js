﻿(function () {

    var alertContainer,
        alertContainerSticky;

    var alertSuccess = "alert-success";
    var alertInfo = "alert-info";
    var alertWarning = "alert-warning";
    var alertDanger = "alert-danger";

    var fadeTime = 400;

    var alertService = {
        showAlert: showAlert,
        success: success,
        info: info,
        warning: warning,
        error: error,
        clearAlerts: clearAlerts
    };

    window.alerts = alertService;

    $(function() {
        alertContainer = $(".alert-container");
        alertContainerSticky = $(".alert-sticky");

        alertContainerSticky.on('close.bs.alert', 'div.alert', function () {
            if (alertContainerSticky.children().length < 3) {
                clearAlerts();
            }
        });
    });
    

    var templateError = _.template("<div class='alert <%= alertClass %> alert-dismissable' role='alert'>" +
		"<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>" +
        "<span><i class='fa fa-times-circle'></i></span>&nbsp;" +
		"<%= message %>" +
		"</div>");

    var templateInfo = _.template("<div class='alert <%= alertClass %> alert-dismissable' role='alert'>" +
		"<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>" +
        "<span><i class='fa fa-info-circle'></i></span>&nbsp;" +
		"<%= message %>" +
		"</div>");

    var templateSuccess = _.template("<div class='alert <%= alertClass %> alert-dismissable' role='alert'>" +
		"<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>" +
        "<span><i class='fa fa-check-circle'></i></span>&nbsp;" +
		"<%= message %>" +
		"</div>");

    var templateWarning = _.template("<div class='alert <%= alertClass %> alert-dismissable' role='alert'>" +
		"<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>" +
        "<span><i class='fa fa-warning'></i></span>&nbsp;" +
		"<%= message %>" +
		"</div>");

    function showAlert(alert) {

        var alertElement;

        if (alert.alertClass === alertSuccess) {
            alertElement = $(templateSuccess(alert));
        } else {
            if (alert.alertClass === alertInfo) {
                alertElement = $(templateInfo(alert));
            } else {
                if (alert.alertClass === alertDanger) {
                    alertElement = $(templateError(alert));
                } else if (alert.alertClass === alertWarning) {
                    alertElement = $(templateWarning(alert));
                }
            }
        }
        var alertElementSticky = alertElement.clone();

        alertContainer.append(alertElement);

        window.setTimeout(function () {
            alertElement.fadeOut(fadeTime, function () {

                process(alertContainerSticky, alertElementSticky);

                if (!alertContainerSticky.is(':visible')) {
                    alertContainerSticky.fadeIn(fadeTime);
                }
            });
        }, 2000);
    }

    function success(message) {
        showAlert({ alertClass: alertSuccess, message: message });
    }

    function info(message) {
        showAlert({ alertClass: alertInfo, message: message });
    }

    function warning(message) {
        showAlert({ alertClass: alertWarning, message: message });
    }

    function error(message) {
        showAlert({ alertClass: alertDanger, message: message });
    }

    function clearAlerts() {
        alertContainer.empty();
        alertContainerSticky.find('#clearAllBtn').off('click');
        alertContainerSticky.fadeOut(fadeTime, function() {
            alertContainerSticky.empty();
        });
    }

    function process(alertContainerSticky, alertElementSticky) {
        if (alertContainerSticky.children().length < 2) {
            alertContainerSticky.append('<div class="clear-alerts-button"><button id="clearAllBtn" class="btn btn-default btn-xs" type="button">Clear All Alerts</button></div>');
            alertContainerSticky.find('#clearAllBtn').on('click', function (e) {
                e.preventDefault();
                clearAlerts();
            });
        }
        alertContainerSticky.append(alertElementSticky);
    }

})();
