﻿var dateHelpers = (function () {

    var addTimeSpanGen = function (timeSpanStep) {

        var originalTimeSpan = new String(timeSpanStep);

        return function (startDate, spanToAdd) {

            if (!angular.isNumber(spanToAdd)) {
                spanToAdd = parseInt(spanToAdd);
            }

            if (!Number.isFinite(spanToAdd))
                return null;

            // weeks are a special case as momentjs does not support it directly
            if (originalTimeSpan.toString() === 'weeks') {
                timeSpanStep = 'days';
                spanToAdd = spanToAdd * 7;
            }
            var clonedDate = moment(startDate).clone();
            return moment(clonedDate).add(spanToAdd, timeSpanStep);
        }
    };

    var dateBeforeMinDate = function (momentDate) {
        return momentDate.isBefore(moment("01-01-" + cc.nineteenHundred, cc.dateFormats.outFormat));
    }

    var addDays = addTimeSpanGen('days');
    var addWeeks = addTimeSpanGen('weeks');
    var addMonths = addTimeSpanGen('months');
    var addYears = addTimeSpanGen('years');

    function processDate(dateValue) {
        if (adnug.existy(dateValue)) {
            return moment(dateValue).toDate();
        }
        return null;
    }
    
    var subtractTimeSpanGen = function (timeSpanStep) {

        var originalTimeSpan = new String(timeSpanStep);

        return function (startDate, spanToSubtract) {

            if (!angular.isNumber(spanToSubtract)) {
                spanToSubtract = parseInt(spanToSubtract);
            }

            if (!Number.isFinite(spanToSubtract))
                return null;

            // weeks are a special case as momentjs does not support it directly
            if (originalTimeSpan.toString() === 'weeks') {
                timeSpanStep = 'days';
                spanToSubtract = spanToSubtract * 7;
            }
            var clonedDate = moment(startDate, cc.dateFormats.outFormat).clone();
            return moment(clonedDate).subtract(spanToSubtract, timeSpanStep);
        }
    };

    var subtractYears = subtractTimeSpanGen('years');

    return {
        addDays: addDays,
        addWeeks: addWeeks,
        addMonths: addMonths,
        addYears: addYears,
        dateBeforeMinDate: dateBeforeMinDate,
        processDate: processDate,
        subtractYears: subtractYears
    }

})();
