﻿adnug.ajax = (function () {

    $(function () {
        var spinner;
        var target = $('#loading-overlay');
        //loadingOverlay.hide();

        var spinnerOptions = {
            radius: 42,
            rotate: 0,
            scale: 1.0,
            shadow: true,
            lines: 13,
            length: 28,
            width: 14,
            speed: 1.0,
            corners: 1.0,
            trail: 60,
            opacity: 0.25,
            color: '#4F81BD'
        };

        var itemInvoked;

        $(document).ajaxStart(function (e) {

            itemInvoked = e.currentTarget.activeElement.name;
            $('[name="'+ itemInvoked + '"]').attr('disabled', 'disabled');


            spinner = new Spinner(spinnerOptions).spin();
            target.append(spinner.el);

            alerts.clearAlerts();
        });

        $(document).ajaxError(function (event, jqxhr, settings, thrownError) {


        });

        $(document).ajaxStop(function (e) {

            $('[name="' + itemInvoked + '"]').removeAttr('disabled');

            spinner.stop();
        });
    });

    return {

    };

}());