﻿(function () {
    'use strict';

    var controllerId = 'HomeController';

    angular.module(cc.appName).controller(controllerId, ['$scope', '$log', 'common', homeController]);

    function homeController($scope, $log, common) {

        $scope.msg = 'Hello ADNUG';


        activate();

        function activate() {
            $log.log('Home page loaded!', null, true);
            common.activateController([], controllerId);
        }
    };
})();