﻿(function () {
    'use strict';

    var controllerId = 'SimpleQueryDemoController';

    angular.module(cc.appName).controller(controllerId, ['$scope', '$log', 'common', 'dataContext', simpleQueryDemoController]);

    function simpleQueryDemoController($scope, $log, common, dataContext) {

        init();

        activate();

        function activate() {
            common.activateController([getLicences()], controllerId).then(function () {
                $log.log('loaded');
            });
        }

        function init() {
            $scope.viewModel = {};
        }

        function getLicences() {

            return dataContext.getLicences().then(function (data) {

                $scope.viewModel.licences = data.data.licences;

            }, function(response) {

            });

        }

    }




})();