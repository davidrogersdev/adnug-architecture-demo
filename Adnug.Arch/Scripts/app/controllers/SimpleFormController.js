﻿(function () {
    'use strict';

    var controllerId = 'SimpleFormController',
        formController = '$formController';

    angular.module(cc.appName).controller(controllerId, ['$scope', '$log', 'common', 'dataContext', simpleFormController]);

    function simpleFormController($scope, $log, common, dataContext) {

        $scope.msg = 'Hello ADNUG';
        init();

        activate();

        function activate() {
            common.activateController([], controllerId).then(function() {

                $scope.simpleForm = $('form[name="simpleFormDemo"]').data(formController);
            });
        }

        function init() {
            $scope.submit = submit;
        }

        function submit($event) {

            $event.preventDefault();

            if (common.checkSubmit($scope.simpleForm)) {

                dataContext.setSimpleFormModel($scope.editModel).then(function(data) {

                    common.alertsService.success('All is well');

                }, function(response) {
                    if (response.status === cc.badRequest) {
                        $log.log('bad');

                        var data = response.data;

                        common.lightUpWithModelStateErrors(data, $scope.simpleForm);
                        var brokenRules = common.getDataErrors(data);
                        common.displayErrors(brokenRules);
                    }
                });

            }
        }
    }


})();