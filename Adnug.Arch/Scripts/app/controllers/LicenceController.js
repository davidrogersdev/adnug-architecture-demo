﻿(function () {
    'use strict';

    var controllerId = 'LicenceController';

    angular.module(cc.appName).controller(controllerId, ['$scope', '$log', 'common', 'config', 'dataContext', licenceController]);

    function licenceController($scope, $log, common, config, dataContext) {

        init();

        activate();

        function activate() {
            common.activateController([primeResource()], controllerId).then(function() {
               
                $scope.editModel = {};
                angular.element(document).ready(function() {
                    $scope.getLicenceForm = $('ng-form[name="getLicenceForm"]').data(cc.formController);
                    $scope.editLicenceForm = $('ng-form[name="editLicenceForm"]').data(cc.formController);
                });
            });
        }

        function init() {
            $scope.submit = submit;
            $scope.submitLicence = submitLicence;
        }

        function submit($event) {

            if (!$scope.licenceRetrieved) {

                $event.preventDefault();

                var licence = dataContext.getLicence($scope.licenceId).then(function (data) {

                    $scope.licenceRetrieved = true;
                    $scope.editModel.licence = data.licence;

                    angular.element(document).ready(function () {
                        $scope.editLicenceForm = $('ng-form[name="editLicenceForm"]').data(cc.formController);
                    });

                }, function(response) {

                });
            } else {
                submitLicence($event);
            }
        }

        function submitLicence($event) {
            $event.preventDefault();

            $scope.editModel.licenceAllocations = null;

            if (common.checkSubmit($scope.editLicenceForm)) {

                dataContext.saveLicence({ licence: $scope.editModel.licence }).then(function (value) {

                    $scope.licenceRetrieved = false;

                    common.alertsService.success('All is well');

                }, function (error) {

                    var data = error.data;

                    if (error.status === cc.badRequest) {

                        common.lightUpWithModelStateErrors(data, $scope.editLicenceForm);
                        var brokenRules = common.getDataErrors(data);
                        common.displayErrors(brokenRules);

                    } else {

                        var errors = data.split(sc.delimiter).filter(i => i.trim() !== '');
                        common.displayErrors(errors);

                    }
                });
            }
        }
        

        function primeResource() {

            var baseUrl = '/api/licence/';

            dataContext.primeResource(baseUrl + ':id');

        }
    }

})();