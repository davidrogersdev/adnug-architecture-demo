/*
(function () {
    'use strict';

    var adnugApp = angular.module(cc.appName);
    var templatesPath = 'Scripts/app/templates';

    // Collect the routes
    adnugApp.constant('routes', getRoutes());

    // Configure the routes and route resolvers
    adnugApp.config(['$routeProvider', 'routes', '$locationProvider', routeConfigurator]);

    function routeConfigurator($routeProvider, routes, $locationProvider) {

        routes.forEach(function (r) {
            setRoute(r.url, r.config);
        });

        $routeProvider.otherwise({ redirectTo: '/' });

        $locationProvider.html5Mode(true);

        function setRoute(url, definition) {

            $routeProvider.when(url, definition);

            return $routeProvider;
        }
    }

    // Define the routes
    function getRoutes() {

        var userRoutes = [
            {
                url: '/',
                config: {
                    templateUrl: templatesPath + '/Home.html',
                    title: 'Home',
                    settings: {
                        nav: 1
                    },
                    controller: 'HomeController'
                }
            }
        ];

        return userRoutes;
    }
})();
*/