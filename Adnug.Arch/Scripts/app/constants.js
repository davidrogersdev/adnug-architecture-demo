﻿var clientConstants = (function () {

    var appName = 'adnugApp';
    var errorMessage = 'errorMessage';
    var formController = '$formController';
    var invalidFormInputMessage = 'Invalid form input.';
    var opResult = {
        success: 'success'
    };

    var badRequest = 400;

    return {
        appName: appName,
        badRequest: badRequest,
        errorMessage: errorMessage,
        formController: formController,
        invalidFormInputMessage: invalidFormInputMessage,
        opResult: opResult
    };
}());

var cc = clientConstants; // alias