﻿(function () {
    'use strict';

    var adnugApp = angular.module(cc.appName);

    var events = {
        controllerActivateSuccess: 'controller.activateSuccess'
    };

    var config = {
        docTitle: 'ADNUG Architecture Demo - ',
        events: events
    };

    adnugApp.value('config', config);

    //#region Configure the common services via commonConfig
    adnugApp.config(['commonConfigProvider', '$httpProvider', function (cfg, $httpProvider) {
        cfg.config.controllerActivateSuccessEvent = config.events.controllerActivateSuccess;

        $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

        // Include AntiForgeryToken to prevent CSRF attacks
        $httpProvider.defaults.headers.common['X-RequestVerificationToken'] = angular.element('input[name="__RequestVerificationToken"]').val();

    }]);
    //#endregion

}());