﻿(function () {
    'use strict';

    // Define the common module 
    // Contains services:
    //  - common
    var commonModule = angular.module('common', []);

    // Must configure the common service and set its 
    // events via the commonConfigProvider
    commonModule.provider('commonConfig', function () {
        this.config = {
            // These are the properties we need to set
            // They are commented out just to show what will be set.

            //controllerActivateSuccessEvent: '',
        };

        this.$get = function () {
            return {
                config: this.config
            };
        };
    });

    commonModule.factory('common',
        ['$q', '$rootScope', '$timeout', 'commonConfig', 'alertsService', 'validationRulesService', common]
        );

    function common($q, $rootScope, $timeout, commonConfig, alertsService, validationRulesService) {

        var service = {
            // common angular dependencies
            $broadcast: $broadcast,
            $q: $q,
            $timeout: $timeout,
            // generic
            activateController: activateController,
            alertsService: alertsService,
            checkSubmit: checkSubmit,
            displayErrors: displayErrors,
            displayValidationError: displayValidationError,
            getDataErrors: getDataErrors,
            lightUpWithModelStateErrors: lightUpWithModelStateErrors,
            prefetchTemplates: prefetchTemplates
        };

        return service;

        function activateController(promises, controllerId) {
            return $q.all(promises).then(function (eventArgs) {
                var data = { controllerId: controllerId };
                $broadcast(commonConfig.config.controllerActivateSuccessEvent, data);
            });
        }

        function $broadcast() {
            return $rootScope.$broadcast.apply($rootScope, arguments);
        }

        function prefetchTemplates($templateCache, templates, dataContext) {

            templates.forEach(function (templ) {

                if ($templateCache.get(templ)) return; //prevent the prefetching if the template is already in the cache

                dataContext.getTemplate(templ).then(function (t) {
                    $templateCache.put(templ, t.data);
                });
            });
        }

        function lightUpWithModelStateErrors(data, form) {

            if (adnug.existy(data)) {

                var fields = Object.keys(data);

                if (adnug.existy(fields)) {
                    fields.forEach(function (field) {
                        if (data[field].errors && data[field].errors.length > 0) {
                            data[field].errors.forEach(function (error) {
                                var errorMessage = error[cc.errorMessage];
                                var errorKey = errorMessage.substringUntil(':').trim();

                                field = field.split('.').join(''); // strip periods

                                var validationObject = validationRulesService.getRulesObjectForField(field);
                                var rule = validationRulesService.getRuleForErrorKey(validationObject, errorKey);

                                if (adnug.existy(rule)) {
                                    form[field].$setTouched();
                                    form[field].$setValidity(errorMessage.substringUntil(':'), false);
                                }
                            });
                        }
                    });
                }
            }
        }

        function getDataErrors(data) {

            var brokenRules = [];

            if (adnug.existy(data)) {

                var fields = Object.keys(data);

                if (adnug.existy(fields)) {
                    fields.forEach(function (field) {
                        if (data[field].errors && data[field].errors.length > 0) {
                            data[field].errors.forEach(function (error) {
                                var errorMessage = error[clientConstants.errorMessage];

                                if ($.inArray(errorMessage, brokenRules) === -1) {
                                    brokenRules.push(errorMessage.substringAfter(':'));
                                }
                            });
                        }
                    });
                }

            }
            return brokenRules;
        }

        function displayErrors(errors) {

            if (adnug.existy(errors)) {
                errors.forEach(function (errorMsg) {
                    alertsService.error(errorMsg.trim());
                });
            }
        }

        function displayValidationError(message) {
            alertsService.error(message);
        }

        function checkSubmit(form) {

            var brokenRules = [];

            if (form.$invalid) {
                angular.forEach(form.$error, function (error) {
                    angular.forEach(error, function (errorField) {
                        if (errorField == null || errorField.$setTouched == null) {
                            return;
                        }

                        errorField.$setTouched(); // by setting to touched, the relevant message will display if the field is invalid.

                        var messages = validationRulesService.getFieldValidationRules(errorField);

                        if (adnug.existy(messages)) {
                            for (var i = 0; i < messages.length; i++) {
                                if ($.inArray(messages[i], brokenRules) === -1) {
                                    brokenRules.push(messages[i]);
                                }
                            }
                        }
                    });
                });
            }

            for (var i = 0; i < brokenRules.length; i++) {
                displayValidationError(brokenRules[i]);
            }

            return brokenRules.length === 0;
        }
    }

}());