﻿(function () {
    var appModule = angular.module(cc.appName);

    appModule.directive("max", max);

    function max($compile) {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, elem, attr, ctrl) {
                scope.$watch(attr.max, function () {
                    ctrl.$setViewValue(ctrl.$viewValue);
                });
                var maxValidator = function (value) {
                    var max = scope.$eval(attr.max) || Infinity;
                    if (!isEmpty(value) && value > max) {
                        ctrl.$setValidity('max', false);
                    } else {
                        ctrl.$setValidity('max', true);
                    }

                    return value;
                };

                ctrl.$parsers.push(maxValidator);
                ctrl.$formatters.push(maxValidator);
            }
        };
    };

    function isEmpty(value) {
        return angular.isUndefined(value) || value === '' || value === null || value !== value;
    }

}());