﻿(function () {

    var appModule = angular.module(cc.appName);

    var hasColon = function () {
        return {
            restrict: "A",
            require: "ngModel",
            link: function (scope, element, attributes, ngModel) {

                ngModel.$validators.adHasColon = function(modelValue) {
                    return adnug.textContains(modelValue, ':');
                };
            }
        };
    };

    appModule.directive("adHasColon", hasColon);

}());