﻿(function () {

    var appModule = angular.module(cc.appName);

    var compareTo = function () {
        return {
            require: "ngModel",
            scope: {
                otherModelValue: "=adCompareto"
            },
            link: function (scope, element, attributes, ngModel) {

                ngModel.$validators.adCompareTo = function (modelValue) {
                    return modelValue == scope.otherModelValue;
                };

                scope.$watch("otherModelValue", function () {
                    ngModel.$validate();
                });
            }
        };
    };

    appModule.directive("adCompareto", compareTo);

}());