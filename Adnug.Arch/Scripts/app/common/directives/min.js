﻿(function () {
    var appModule = angular.module(cc.appName);

    appModule.directive("min", min);

    function min($compile) {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, elem, attr, ctrl) {
                scope.$watch(attr.min, function () {
                    ctrl.$setViewValue(ctrl.$viewValue);
                });
                var minValidator = function (value) {
                    var min = scope.$eval(attr.min) || 0;
                    if (!isEmpty(value) && value < min) {
                        ctrl.$setValidity('min', false);
                    } else {
                        ctrl.$setValidity('min', true);
                    }

                    return value;
                };

                ctrl.$parsers.push(minValidator);
                ctrl.$formatters.push(minValidator);
            }
        };
    };

    function isEmpty(value) {
        return angular.isUndefined(value) || value === '' || value === null || value !== value;
    }

}());