﻿(function () {

    var appModule = angular.module(cc.appName);

    appModule.factory(cc.appName, ['$compile', 'validationRulesService', forminput]);

    var setupDom = function (element) {

        var input = element.querySelector("input, textarea, select");
        var type = input.getAttribute("type");
        var name = input.getAttribute("name");

        if (type !== "checkbox" && type !== "radio") {
            input.classList.add("form-control");
        }

        var label = element.querySelector("label");

        if (adnug.existy(label)) {
            label.classList.add("control-label");
        }

        element.classList.add("form-group");

        return name;
    };

    var addMessages = function (form, element, name, $compile, scope, validationRulesService) {

        var formName = form.$name + "." + name;

        // By making the "ng-if" depend on whether it is invalid and has been touched, the 
        // element does not show the invalid message where the user has not even got to the input yet
        // or if they have, only when the element is invalid.
        var messages = "<div class='help-block' ng-messages='" +
                        formName + ".$error" +
                        "' ng-if='" + formName + ".$invalid && " + formName + ".$touched'>" +
                        addCustomValidationMessage(name, validationRulesService) +
                        "<div ng-messages-include='/Scripts/app/templates/messages/messages.html'></div>" +
                        "</div>";

        element.append($compile(messages)(scope));
    };

    var addCustomValidationMessage = function (fieldName, validationRulesService) {

        var rules = validationRulesService.getValidationRules(fieldName);

        var message = '';

        if (adnug.existy(rules)) {

            for (var i = 0; i < rules.length; i++) {
                if (rules[i].addToHelpBlock) {
                    message += "<div ng-message='" + rules[i].name + "'>" + rules[i].message + "</div>";
                }
            }

        }

        return message;
    };

    var watcherFor = function (form, name) {
        return function () {
            if (name && form[name]) {
                return form[name].$invalid;
            }
        };
    };

    var link = function ($compile, validationRulesService) {
        return function (scope, element, attributes, form) {
            var name = setupDom(element[0]);

            addMessages(form, element, name, $compile, scope, validationRulesService);
            scope.$watch(watcherFor(form, name));
        };
    };

    function forminput($compile, validationRulesService) {

        return {
            restrict: "A",
            require: "^form",
            link: link($compile, validationRulesService)
        };

    };

    appModule.directive("adFormInput", forminput);

}());
