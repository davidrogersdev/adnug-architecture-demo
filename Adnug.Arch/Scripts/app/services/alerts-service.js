﻿(function () {
    'use strict';

    var serviceId = 'alertsService';

    angular.module(cc.appName).factory(serviceId, [alertsService]);

    function alertsService() {
        return window.alerts;
    };

})();
