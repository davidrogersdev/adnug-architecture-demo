﻿(function () {
    'use strict';

    var serviceId = 'dataContext';

    angular.module(cc.appName).factory(serviceId, ['$http', '$resource', 'common', dataContext]);

    function dataContext($http, $resource, common) {

        var resource;

        var setSimpleFormModel = function(model) {
            return $http.post('/Home/SimpleForm', model);
        };

        var getLicence = function(id, callback) {
            return resource.get({id: id}).$promise;
        };

        var getLicences = function() {
            return $http.get('/api/licensing');
        };

        var saveLicence = function(licence) {
            return resource.save(licence).$promise;
        };

        var primeResource = function (url, params) {
            resource = $resource(url, params);
        }


        var service = {
            getLicences: getLicences,
            getLicence: getLicence,
            primeResource: primeResource,
            saveLicence: saveLicence,
            setSimpleFormModel: setSimpleFormModel
        };

        return service;
    };


})();
