﻿(function () {

    'use strict';

    var serviceId = 'validationRulesService';

    angular.module(cc.appName).factory(serviceId, [validationRulesService]);

    function validationRulesService() {

        var validationRules = [
            {
                fieldName: 'simpleFormModelFirstName',
                rules: [
                    { name: 'required', message: 'Name is required', addToHelpBlock: false },
                    { name: 'minlength', message: 'Length of name must be 2 or 3 characters', addToHelpBlock: false },
                    { name: 'maxlength', message: 'Length cannot be greater than 3 characters', addToHelpBlock: true }
                    //{ name: 'endInXRule', message: 'First Name end with the letter x.', addToHelpBlock: true }
                ]
            },
            {
                fieldName: 'licenceEditModelLicenceSoftwareNameAndDescription',
                rules: [
                    { name: 'required', message: 'The Name and Description field is required.', addToHelpBlock: true },
                    { name: 'adHasColon', message: 'The Name and Description field must have a colon.', addToHelpBlock: true }
                ]
            },
            {
                fieldName: 'licenceEditModelLicenceLicenceKey',
                rules: [
                    { name: 'required', message: 'The Licence Key field is required.', addToHelpBlock: true }
                ]
            }
        ];

        var getValidationRules = function (fieldName) {

            for (var i = 0; i < validationRules.length; i++) {
                if (ruleExists(validationRules[i], fieldName)) {
                    return validationRules[i].rules;
                }
            }

            return null;
        };

        var getFieldValidationRules = function (field) {
            var brokenRules = [];
            for (var i = 0; i < validationRules.length; i++) {
                if (ruleExists(validationRules[i], field.$name))
                    for (var key in field.$error) {
                        if (field.$error.hasOwnProperty(key)) {

                            var rule = _.find(validationRules[i].rules, function (r) {
                                return r.name === key;
                            });

                            if (adnug.existy(rule)) {
                                brokenRules.push(rule.message);
                            } else if (key === 'required') {
                                brokenRules.push(field.$name + ' is required');
                            }
                        }
                    }
            }

            return brokenRules.length > 0 ? brokenRules : null;
        };

        var ruleExists = function (rule, fieldName) {
            return rule.fieldName === fieldName || (adnug.existy(rule.alternateFieldNames)
                    && $.inArray(fieldName, rule.alternateFieldNames) !== -1);
        };

        var getRulesObjectForField = function(field) {

            var validationOjbect = _.find(validationRules, function (r) {
                return r.fieldName === field;
            });

            return validationOjbect;
        };

        var getRuleForErrorKey = function(validationObject, errorKey) {
            
            var validationOjbect = _.find(validationObject.rules, function (r) {
                return r.name === errorKey;
            });

            return validationOjbect;
        };

        return {
            getFieldValidationRules: getFieldValidationRules,
            getRuleForErrorKey: getRuleForErrorKey,
            getRulesObjectForField: getRulesObjectForField,
            getValidationRules: getValidationRules
        };
    };

})();