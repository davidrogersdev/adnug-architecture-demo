﻿(function () {
    'use strict';

    var adnugApp = angular.module(cc.appName, [
        // Angular modules 
        'ngAnimate',        // animations
        'ngRoute',          // routing
        'ngResource',       // RESTful data retrieval
        'ngMessages',       // Validation messages
        'ngSanitize',       // Validation messages

        // Custom modules 
        'common', // common functions

    ]).config(function ($animateProvider) {
        // This disables ng-animate on elements with the 'no-animate' class.
        // See http://davidchin.me/blog/disable-nganimate-for-selected-elements/
        $animateProvider.classNameFilter(/!no-animate/);
    });

    // Handle routing errors and success events
    adnugApp.run(['$http', '$route', '$rootScope', '$q'/*, 'routeOverlord',*/,
        function ($http, $route, $rootScope, $q/*, routeOverlord*/) {
            // Include $route to kick start the router.
            //routeOverlord.setRoutingHandlers();

        }]);

}());