using Adnug.Arch.Core.DataTransferObjects;
using Adnug.Arch.Validation.Models.ViewModels;
using FluentValidation.Attributes;

namespace Adnug.Arch.Models.ViewModels
{
    [Validator(typeof(LicenceEditModelValidator))]
    public class LicenceEditModel
    {
        public LicenceDto Licence { get; set; }
    }
}