﻿using System.Collections.Generic;
using Adnug.Arch.Core.DataTransferObjects;

namespace Adnug.Arch.Models.ViewModels
{
    public class LicencesViewModel
    {
        public IEnumerable<LicenceDto> Licences { get; set; }
    }
}