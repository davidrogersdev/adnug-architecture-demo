﻿using Adnug.Arch.Validation.Models;
using FluentValidation.Attributes;

namespace Adnug.Arch.Models
{
    [Validator(typeof(SimpleFormModelValidator))]
    public class SimpleFormModel
    {
        public string FirstName { get; set; }
    }
}