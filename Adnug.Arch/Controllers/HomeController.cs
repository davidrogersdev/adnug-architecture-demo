﻿using System.Net;
using System.Web.Mvc;
using Adnug.Arch.Messaging.Queries;
using Adnug.Arch.Models;
using Adnug.Arch.Mvc.Controllers;
using Adnug.Arch.Mvc.Infrastructure.Alerts;
using Adnug.Arch.Mvc.Infrastructure.Logging;
using MediatR;

namespace Adnug.Arch.Controllers
{
    public class HomeController : SiteController
    {
        public HomeController(ILogger logger, IMediator mediator) 
            : base(logger, mediator)
        {
        }

        public ActionResult Index()
        {
            return View();
            /*.WithSuccess("You arrived")
                .WithInfo("Well done")
                .WithWarning("Be warned");*/
        }

        public ActionResult SimpleForm()
        {

            ViewBag.Message = "";

            var simpleFormModel = new SimpleFormModel { FirstName = "Ian" };

            return View(simpleFormModel);
        }

        [HttpPost]
        public ActionResult SimpleForm(SimpleFormModel simpleFormModel)
        {
            // do nothing

            return JsonSuccess(simpleFormModel);
        }

        public ActionResult SimpleQuery()
        {
            return View();
        }
    }
}