﻿using System.Web.Http;
using Adnug.Arch.Messaging.Queries;
using Adnug.Arch.Models.ViewModels;
using Adnug.Arch.Mvc.Controllers;
using Adnug.Arch.Mvc.Infrastructure.Logging;
using MediatR;

namespace Adnug.Arch.Controllers.api
{
    public class LicensingController : ApiSiteController
    {

        public LicensingController(ILogger logger, IMediator mediator)
            : base(logger, mediator)
        {
        }

        [Route("api/licensing")]
        public IHttpActionResult Get()
        {
            LicencesViewModel licencesViewModel = null;

            if (Execute(() => licencesViewModel = _mediator.Send(new GetLicencesQuery())))
            {
                return JsonSuccess(licencesViewModel);
            }

            return JsonError(Errors);
        }
    }
}