﻿using System.Web.Http;
using Adnug.Arch.Messaging.Commands;
using Adnug.Arch.Messaging.Queries;
using Adnug.Arch.Models.ViewModels;
using Adnug.Arch.Mvc.Controllers;
using Adnug.Arch.Mvc.Infrastructure.Logging;
using MediatR;

namespace Adnug.Arch.Controllers.api
{
    public class LicenceController : ApiSiteController
    {
        public LicenceController(ILogger logger, IMediator mediator)
            : base(logger, mediator)
        {
            
        }

        [Route("api/licence/{id}")]
        public IHttpActionResult Get(int? id)
        {
            if (!id.HasValue)
                return JsonError("No id was sent to the server");

            GetLicenceQuery getLicenceQuery = new GetLicenceQuery { LicenceId = id.Value };
            LicenceEditModel licenceEditModel = null;

            if (Authorize("View", "AdnugProtectedResource") && Execute(() => licenceEditModel = _mediator.Send(getLicenceQuery)))
            {
                _logger.LogWarning("done");
                return JsonSuccess(licenceEditModel);
            }

            return JsonError(Errors);
        }

        [Route("api/licence")]
        public IHttpActionResult Post(LicenceEditModel licenceEditModel)
        {
            var updateLicenceCommand = new UpdateLicenceCommand { LicenceEditModel = licenceEditModel };
            int licenceId = -1;

            if (Execute(() => _mediator.Send(updateLicenceCommand)))
            {
                return JsonSuccess(licenceEditModel);
            }

            return JsonError(Errors);
        }
    }
}