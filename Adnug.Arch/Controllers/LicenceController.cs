﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Adnug.Arch.Mvc.Controllers;
using Adnug.Arch.Mvc.Infrastructure.Logging;
using MediatR;

namespace Adnug.Arch.Controllers
{
    public class LicenceController : SiteController
    {
        public LicenceController(ILogger logger, IMediator mediator) 
            : base(logger, mediator)
        {
        }

        public ActionResult Index()
        {
            return View();
        }
    }
}