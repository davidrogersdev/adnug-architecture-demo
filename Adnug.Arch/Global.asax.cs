﻿using System;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Adnug.Arch.App_Start;
using Adnug.Arch.Mvc.Tasks;

namespace Adnug.Arch
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            GlobalConfiguration.Configure(WebApiConfig.Register);
            GlobalConfiguration.Configure(FluentValidationInitializer.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        public void Application_BeginRequest()
        {
            var container = SimpleInjectorInitializer.Container;

            container.Options.DefaultScopedLifestyle.WhenScopeEnds(container, () =>
            {
                foreach (var task in container.GetAllInstances<IRunAfterEachRequest>())
                {
                    task.Execute();
                }
            });

            foreach (var task in container.GetAllInstances<IRunOnEachRequest>())
            {
                task.Execute();
            }
        }

        public void Application_Error(object sender, EventArgs args)
        {
            try
            {
                foreach (var task in DependencyResolver.Current.GetServices(typeof(IRunOnError)).Cast<IRunOnError>())
                {
                    task.Execute();
                }
            }
            catch (Exception)
            {
                // Special case where User is null on HttpContext.Current.User. An example is loading a url with illegal characters in the path.
                //var handleIt = new FinalExceptionHandler(new ErrorResponseCommand(), new HttpContextWrapper(HttpContext.Current));
                //handleIt.Execute();
            }
        }

        public void Application_EndRequest()
        {

        }

        public void Application_AcquireRequestState()
        {
#if DEBUG
            // verifying at this late stage because HttpContext.Current.User is not populated until now and an error is thrown in 
            // MvcRegistry.RegisterMvcParts if that object is null. 
            SimpleInjectorInitializer.Container.Verify();
#endif
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            // Initialize the session with a value (any value), otherwise, the SessionID will be different for every request.
            Session["init"] = 0;
        }

        protected void Application_PreSendRequestHeaders(object sender, EventArgs e)
        {
            // Security measure. The less info about IIS in the headers, the better.
            HttpContext.Current.Response.Headers.Remove("Server");
        }
    }
}
