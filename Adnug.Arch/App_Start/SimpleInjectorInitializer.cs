[assembly: WebActivator.PostApplicationStartMethod(typeof(Adnug.Arch.App_Start.SimpleInjectorInitializer), "Initialize")]

namespace Adnug.Arch.App_Start
{
    using System.Reflection;
    using System.Web.Mvc;
    using SimpleInjector;
    using SimpleInjector.Integration.Web;
    using SimpleInjector.Integration.Web.Mvc;
    using System.Web.Http;
    using Core.ApplicationServices;
    using Infrastructure.Ioc;
    using Mvc.Infrastructure.ModelMetadata;
    using SimpleInjector.Integration.WebApi;


    public static class SimpleInjectorInitializer
    {
        public static Container Container { get; private set; }

        /// <summary>Initialize the container and register it as MVC3 Dependency Resolver.</summary>
        public static void Initialize()
        {
            var container = new Container();
            container.Options.DefaultScopedLifestyle = new WebRequestLifestyle(); // this is fine for WebAPI as well b/c no plans to run it outside of IIS in this project.

            var webAssembly = typeof(SimpleInjectorInitializer).Assembly; // Adnug.Arch assembly

            InitializeContainer(container, webAssembly);

            container.RegisterMvcControllers(webAssembly);
            container.RegisterWebApiControllers(GlobalConfiguration.Configuration);

            // Set the different DI hooks for MVC and WebAPI
            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container)); // for MVC
            GlobalConfiguration.Configuration.DependencyResolver = new SimpleInjectorWebApiDependencyResolver(container); // for WebAPI

            Container = container;
        }

        private static void InitializeContainer(Container container, Assembly webAssembly)
        {
            var mvcAssembly = typeof(IModelMetadataFilter).Assembly; // Adnug.Arch.Mvc assembly.
            var coreAssembly = typeof(ILicenceDataService).Assembly; // Adnug.Arch.Core assembly.

            container.RegisterMvcParts(mvcAssembly);
            container.RegisterEfParts();
            container.RegisterMediatrParts(webAssembly);
            container.RegisterAutomapperParts(webAssembly);
            container.RegisterFluentValidation(webAssembly, coreAssembly);
            container.RegisterTasks(webAssembly, mvcAssembly);
            container.RegisterLoggingServices();
            container.RegisterApplicationServices(coreAssembly);
        }
    }
}