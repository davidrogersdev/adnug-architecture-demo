﻿using System.Data.Entity;
using System.Web.Helpers;
using System.Web.Mvc;
using Adnug.Arch.App_Start;
using Adnug.Arch.DataAccess.Db;
using Adnug.Arch.Mvc.ModelBinders;
using WebActivator;

[assembly: PostApplicationStartMethod(typeof(MiscInitializer), "Initialize")]

namespace Adnug.Arch.App_Start
{
    public static class MiscInitializer
    {
        public static void Initialize()
        {
            // Remove MVC Header - X-AspNetMvc-Version - good security. Hackers don't need to know what version of MVC is on the server.
            MvcHandler.DisableMvcResponseHeader = true;

            // Some Web Servers like NGINX strip the header when they contain an underscore, which are detected as a threat and marked 
            // as invalid and it consequently drops them. So, always use AntiForgeryConfig.CookieName which will return this string
            AntiForgeryConfig.CookieName = "X-RequestVerificationToken";

            // Use a ModelBinder which provides empty collections instead of nulls where Models contain collections as properties.
            ModelBinders.Binders.DefaultBinder = new JimmyBogardDefaultModelBinder();

            // Initialize the Database
            Database.SetInitializer(new LicenceTrackerInitializer());
        }
    }
}