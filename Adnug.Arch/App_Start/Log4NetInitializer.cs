﻿using Adnug.Arch.App_Start;
using log4net.Config;
using WebActivator;

[assembly: PostApplicationStartMethod(typeof(Log4NetInitializer), "Initialize")]

namespace Adnug.Arch.App_Start
{
    public static class Log4NetInitializer
    {
        /// <summary>Initialize Log4Net.</summary>
        public static void Initialize()
        {
            // Add logging
            XmlConfigurator.Configure();
        }
    }
}