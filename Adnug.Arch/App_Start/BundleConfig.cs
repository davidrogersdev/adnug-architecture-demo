﻿using System.Web.Optimization;

namespace Adnug.Arch
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include("~/Scripts/modernizr-*"));

            bundles.Add(new Bundle("~/bundles/libraries").Include(
                "~/Scripts/app/constants.js",
                "~/Scripts/jquery-{version}.js",
                "~/Scripts/moment.js",
                "~/Scripts/moment-with-locales.js",
                "~/Scripts/underscore.js",
                "~/Scripts/adnug/utilities/bootstrap-alert.js",
                "~/Scripts/adnug/utilities/date-helper.js",
                "~/Scripts/respond.js",
                "~/Scripts/bootstrap.js",

                // *************** Angular libs *************** //
                "~/Scripts/angular.js",
                "~/Scripts/angular-route.js",
                "~/Scripts/angular-animate.js",
                "~/Scripts/angular-resource.js",
                "~/Scripts/angular-sanitize.js",
                "~/Scripts/angular-messages.js",

                // *************** Bootstrapping *************** //
                "~/Scripts/app/app.js",
                "~/Scripts/app/config.js",
                "~/Scripts/app/config.route.js",

                // *************** Common Modules *************** //
                "~/Scripts/app/common/common.js",

                // *************** Services *************** //
                "~/Scripts/app/services/alerts-service.js",
                "~/Scripts/app/services/validation-rules-service.js",
                "~/Scripts/app/services/data-context.js",
                //"~/Scripts/app/services/route-overlord.js",

                // *************** Directives *************** //
                "~/Scripts/app/common/directives/form-input.js",
                "~/Scripts/app/common/directives/max.js",
                "~/Scripts/app/common/directives/min.js",
                "~/Scripts/app/common/directives/compare-password.js",
                "~/Scripts/app/common/directives/has-colon.js",

                // *************** Controllers *************** //
                "~/Scripts/app/controllers/HomeController.js",
                "~/Scripts/app/controllers/SimpleFormController.js",
                "~/Scripts/app/controllers/SimpleQueryDemoController.js",
                "~/Scripts/app/controllers/LicenceController.js"

                ));

            bundles.Add(new ScriptBundle("~/bundles/app").Include(
                "~/Scripts/adnug/adnug.js",
                "~/Scripts/adnug/adnug.ajax.js"
                )
            );

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/font-awesome.css",
                      "~/Content/site.css",
                      "~/Content/print.css")
                      );
        }
    }
}
