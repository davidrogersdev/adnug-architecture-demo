﻿using System.Web.Http;
using FluentValidation.Mvc;
using WebApi = FluentValidation.WebApi;

namespace Adnug.Arch.App_Start
{
    public static class FluentValidationInitializer
    {
        /// <summary>Initialize Fluent Validation.</summary>
        public static void Register(HttpConfiguration config)
        {
            // Hook in FluentValidation
            FluentValidationModelValidatorProvider.Configure();

            // Hook in FluentValidation
            WebApi.FluentValidationModelValidatorProvider.Configure(config);
        }
    }
}