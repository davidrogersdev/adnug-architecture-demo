﻿using System.Web.Mvc;
using Adnug.Arch.Mvc.ActionFilters;

namespace Adnug.Arch
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new ValidatorActionFilter()); // custom validation attribute
        }
    }
}
