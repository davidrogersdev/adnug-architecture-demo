﻿using System;
using Adnug.Arch.Core.ApplicationServices;
using Adnug.Arch.Core.DataTransferObjects;
using Adnug.Arch.Messaging.CommandHandlers;
using Adnug.Arch.Messaging.Commands;
using Adnug.Arch.Models.ViewModels;
using FakeItEasy;
using Shouldly;

namespace Adnug.Arch.Tests.LicenceTests
{
	public class LicenceDataTests : IDisposable
	{
		private UpdateLicenceCommand _updateLicenceCommand;

		public void ShouldReturnNegativeValueWhenNoRowsAreUpdated()
		{
			var licenceDataService = A.Fake<ILicenceDataService>();
			var licenceDto = new LicenceDto { Id = 1, LicenceKey = string.Empty };

			//	Fake it such that UpdateLicence returns -1 (meaning no rows are updated).
			A.CallTo(() => licenceDataService.UpdateLicence(licenceDto)).Returns(-1);

			_updateLicenceCommand = new UpdateLicenceCommand
			{
				LicenceEditModel = new LicenceEditModel { Licence = licenceDto }
			};

			var updateLicenceCommandHandler = new UpdateLicenceCommandHandler(licenceDataService);

			var result = updateLicenceCommandHandler.Handle(_updateLicenceCommand);

			result.ShouldBeNegative();
		}

		public void Dispose()
		{

		}
	}
}
